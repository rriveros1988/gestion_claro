<?php
    //ini_set('display_errors', 'On');
    require('../model/consultas.php');
    session_start();

    if(count($_POST) > 0){
        $row = '';
        $rutUsuario = '';
        if (array_key_exists('rutUser', $_SESSION)) {
            $rutUsuario = $_SESSION['rutUser'];
            $cliente = $_POST['cliente'];
            $comuna = $_POST['comuna'];
            $calle = $_POST['calle'];
            $numero = $_POST['numero'];
            $fecha = $_POST['fecha'];
            $fono1 = $_POST['fono1'];
            $fono2 = $_POST['fono2'];
            $hora = $_POST['hora'];
            $rango = $_POST['rango'];
            $region = $_POST['region'];
            $rut = $_POST['rut'];
            $arrayOTDth = $_POST['arrayOTDth'];

            $folios = '';

            for($i= 0 ; $i < count($arrayOTDth); $i++){
              $row = actualizarAgendaOTDTH($arrayOTDth[$i], $cliente, $comuna, $numero, $calle, $fecha, $fono1, $fono2, $hora, $rango, $region, $rut);
              if($i === 0){
                $folios = $arrayOTDth[$i];
              }
              else{
                $folios = $folios . "," . $arrayOTDth[$i];
              }
            }

            if($row != "Error" )
            {
              $row2 = datosOtDTHModificado($folios);

              if(is_array($row2))
              {
                  $results = array(
                      "sEcho" => 1,
                      "iTotalRecords" => count($row2),
                      "iTotalDisplayRecords" => count($row2),
                      "aaData"=>$row2
                  );
                  echo json_encode($results);
              }
              else{
                  $results = array(
                      "sEcho" => 1,
                      "iTotalRecords" => 0,
                      "iTotalDisplayRecords" => 0,
                      "aaData"=>[]
                  );
                  echo json_encode($results);
              }
            }
            else{
                echo "Sin datos";
            }
        }
        else{
        	echo "Sin datos";
        }
    }
    else{
        echo "Sin datos";
    }
?>
