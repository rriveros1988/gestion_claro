<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) == 0){
		$row = checkToken($_COOKIE['tk_w_o']);
		if($row !== NULL){
			echo "TOKEN_SI";
		}
		else{
			borraTokenLogin($_SESSION['rutUser']);
			session_destroy();
			echo "TOKEN_NO";
		}
	}
	else{
		echo "Sin datos";
	}
?>
