<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'VISTA_OT_DTH_TODOS';

// Table's primary key
$primaryKey = 'FOLIO';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => 'S', 'dt' => 'S'),
  array( 'db' => 'TECNOLOGIA', 'dt' => 'TECNOLOGIA'),
  array( 'db' => 'FOLIO', 'dt' => 'FOLIO'),
  array( 'db' => 'ORIGEN', 'dt' => 'ORIGEN'),
  array( 'db' => 'TIPO_TICKET', 'dt' => 'TIPO_TICKET'),
  array( 'db' => 'ESTADO_MANDANTE', 'dt' => 'ESTADO_MANDANTE'),
  array( 'db' => 'FECHA_CREACION', 'dt' => 'FECHA_CREACION'),
  array( 'db' => 'FECHA_AGENDA', 'dt' => 'FECHA_AGENDA'),
  array( 'db' => 'HORA_AGENDA', 'dt' => 'HORA_AGENDA'),
  array( 'db' => 'RANGO', 'dt' => 'RANGO'),
  array( 'db' => 'DIAS', 'dt' => 'DIAS'),
  array( 'db' => 'DIAS_AGENDA', 'dt' => 'DIAS_AGENDA'),
  array( 'db' => 'CANTIDAD_REAGENDAMIENTOS', 'dt' => 'CANTIDAD_REAGENDAMIENTOS'),
  array( 'db' => 'COSTO', 'dt' => 'COSTO'),
  array( 'db' => 'REGION', 'dt' => 'REGION'),
  array( 'db' => 'COMUNA', 'dt' => 'COMUNA'),
  array( 'db' => 'RUT', 'dt' => 'RUT'),
  array( 'db' => 'CLIENTE', 'dt' => 'CLIENTE'),
  array( 'db' => 'DIRECCION', 'dt' => 'DIRECCION'),
  array( 'db' => 'FONO1', 'dt' => 'FONO1'),
  array( 'db' => 'FONO2', 'dt' => 'FONO2'),
  array( 'db' => 'ESTADO_DESPACHO', 'dt' => 'ESTADO_DESPACHO'),
  array( 'db' => 'DESPACHO', 'dt' => 'DESPACHO'),
  array( 'db' => 'TECNICO', 'dt' => 'TECNICO'),
  array( 'db' => 'REFERENCIA', 'dt' => 'REFERENCIA'),
  array( 'db' => 'OBSERVACION_DESPACHO', 'dt' => 'OBSERVACION_DESPACHO'),
  array( 'db' => 'REFERENCIA2', 'dt' => 'REFERENCIA2'),
  array( 'db' => 'OBSERVACION_DESPACHO2', 'dt' => 'OBSERVACION_DESPACHO2'),
  array( 'db' => 'BANDEJA', 'dt' => 'BANDEJA'),
  array( 'db' => 'CALLE', 'dt' => 'CALLE'),
  array( 'db' => 'NUMERO', 'dt' => 'NUMERO')
);

// SQL server connection information
$sql_details = array(
    'user' => 'rriveros_generico_sci',
    'pass' => 'Rodrigo2015rr@',
    'db'   => 'rriveros_gestion_claro',
    'host' => '200.63.97.58'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
	SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns )
);
