<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');

	//Login
	function consultaUsuarioConectado($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT
CASE WHEN TIMESTAMPDIFF(SECOND , TOKEN_WEB_TIME, NOW()) < 300 AND TOKEN_WEB IS NOT NULL AND LENGTH(TOKEN_WEB) > 1  THEN 'SI' ELSE 'NO' END 'CHECK'
FROM USUARIO
WHERE RUT = '{$rut}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuario($rut, $pass){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.NOMBRE, U.RUT, U.IDPERFIL, U.ESTADO, P.NOMBRE 'PERFIL'
FROM USUARIO U
LEFT JOIN PERFIL P
ON U.IDPERFIL  = P.IDPERFIL
WHERE RUT = '" . $rut . "' AND PASS = '" . $pass . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Refresh
	function checkUsuarioSinPass($token){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.NOMBRE, U.RUT, U.IDPERFIL, U.ESTADO, P.NOMBRE 'PERFIL'
FROM USUARIO U
LEFT JOIN PERFIL P
ON U.IDPERFIL  = P.IDPERFIL
WHERE U.TOKEN_WEB = '{$token}'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Ingresa Token Login
	function actualizaTokenLogin($rut, $token){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = '{$token}',
TOKEN_WEB_TIME = NOW()
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Chequeo Token
	function checkToken($token){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT
FROM USUARIO
WHERE TIMESTAMPDIFF(SECOND , TOKEN_WEB_TIME, NOW()) < 300
AND TOKEN_WEB  = '{$token}'
AND TOKEN_WEB <> ''";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Borra Token Login
	function borraTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Areas padres
	function consultaAreasComunesPadreSolo($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PADRE, ICONOPADRE, TEXTOPADRE
FROM AREAWEB
WHERE NOMBRE IN
(
SELECT A.NOMBRE
FROM AREAWEB A
LEFT JOIN PERMISOS P
ON A.IDAREAWEB = P.IDAREAWEB
LEFT JOIN PERFIL  R
ON P.IDPERFIL = R.IDPERFIL
LEFT JOIN USUARIO U
ON R.IDPERFIL = U.IDPERFIL
WHERE A.TIPO = 0
AND U.RUT = '{$rut}'
)
GROUP BY PADRE, ICONOPADRE, TEXTOPADRE, ORDEN
ORDER BY ORDEN ASC";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaAreasComunesPadre($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT PADRE, NOMBRE, TIPO, ICONO, RUTA, TEXTO, ORDEN, SUBORDEN
FROM AREAWEB
WHERE NOMBRE IN
(
SELECT A.NOMBRE
FROM AREAWEB A
LEFT JOIN PERMISOS P
ON A.IDAREAWEB = P.IDAREAWEB
LEFT JOIN PERFIL  R
ON P.IDPERFIL = R.IDPERFIL
LEFT JOIN USUARIO U
ON R.IDPERFIL = U.IDPERFIL
WHERE A.TIPO = 0
AND U.RUT = '{$rut}'
)
UNION ALL
SELECT DISTINCT PADRE, NOMBRE, TIPO, ICONO, RUTA, TEXTO, ORDEN, SUBORDEN
FROM AREAWEB
WHERE TIPO = 1
ORDER BY ORDEN ASC, SUBORDEN ASC";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreasComunes(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB
	WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Personal interno
	function consultaPersonal(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL PERSONAL";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Destruye Token Login
	function destruyeTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL,
TOKEN_WEB_TIME = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Select mano de obra diferentes
	function consultaManoDeObraPersonalInterno(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT 'Todos' CLASIFICACION
UNION ALL
SELECT DISTINCT CLASIFICACION
FROM PERSONAL
WHERE EXTERNO = 0";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select centro de costo
	function consultaCentroCostoPersonalInterno(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT 'Todos' CENTRO_COSTO
UNION ALL
SELECT DISTINCT C.CENTRO_COSTO
FROM PERSONAL P
LEFT JOIN PERSONAL_ESTADO PE
ON P.IDPERSONAL = PE.IDPERSONAL
AND DATE_FORMAT(NOW(), '%Y-%m-%d') >= PE.FECHA_INICIO
AND (DATE_FORMAT(NOW(), '%Y-%m-%d') <= PE.FECHA_TERMINO
	OR PE.FECHA_TERMINO IS NULL)
LEFT JOIN PERSONAL_ESTADO_CONCEPTO CPE
ON PE.IDPERSONAL_ESTADO_CONCEPTO = CPE.IDPERSONAL_ESTADO_CONCEPTO
LEFT JOIN ACT A
ON P.IDPERSONAL  = A.IDPERSONAL
LEFT JOIN  CENTRO_COSTO C
ON A.IDCENTRO_COSTO  = C.IDCENTRO_COSTO
WHERE P.EXTERNO = 0
AND C.CENTRO_COSTO IS NOT NULL
AND ((CPE.PERSONAL_ESTADO_CONCEPTO <> 'Desvinculado'
AND CPE.PERSONAL_ESTADO_CONCEPTO <> 'Renuncia')
OR CPE.PERSONAL_ESTADO_CONCEPTO IS NULL)";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select estado de RRHH
	function consultaRRHHPersonalInterno(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT 'Todos' ESTADO_RRHH
UNION ALL
SELECT DISTINCT
CASE WHEN CPE.PERSONAL_ESTADO_CONCEPTO IS NULL THEN 'Vigente' ELSE CPE.PERSONAL_ESTADO_CONCEPTO END 'ESTADO_RRHH'
FROM PERSONAL P
LEFT JOIN PERSONAL_ESTADO PE
ON P.IDPERSONAL = PE.IDPERSONAL
AND DATE_FORMAT(NOW(), '%Y-%m-%d') >= PE.FECHA_INICIO
AND (DATE_FORMAT(NOW(), '%Y-%m-%d') <= PE.FECHA_TERMINO
	OR PE.FECHA_TERMINO IS NULL)
LEFT JOIN PERSONAL_ESTADO_CONCEPTO CPE
ON PE.IDPERSONAL_ESTADO_CONCEPTO = CPE.IDPERSONAL_ESTADO_CONCEPTO
WHERE P.EXTERNO = 0
AND ((CPE.PERSONAL_ESTADO_CONCEPTO <> 'Desvinculado'
AND CPE.PERSONAL_ESTADO_CONCEPTO <> 'Renuncia')
OR CPE.PERSONAL_ESTADO_CONCEPTO IS NULL)";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Estado Civil
	function consultaEstadoCivil(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM ESTADO_CIVIL
ORDER BY ESTADO_CIVIL";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Nacionalidad
	function consultaNacionalidad(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM NACIONALIDAD
ORDER BY NACIONALIDAD";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Area Funcional
	function consultaAreaFuncional(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDAREAFUNCIONAL, COMUNA, PROVINCIA, REGION
FROM AREAFUNCIONAL";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Nivel Estudios
	function consultaNivelEstudios(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM NIVEL_EDUCACIONAL";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Ceco
	function consultaCeco(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM CENTRO_COSTO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select contrato depto
	function consultaContratoDepto(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM SERVICIO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select contrato depto
	function consultaCliente($idcontratodepto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT C.CLIENTE, C.IDCLIENTE
FROM CLIENTE C
LEFT JOIN ESTRUCTURA_OPERACION O
ON C.IDCLIENTE = O.IDCLIENTE
WHERE O.IDSERVICIO = '{$idcontratodepto}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select actividad
	function consultaActividad($idcontratodepto, $idcliente){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT A.ACTIVIDAD, A.IDACTIVIDAD
FROM ACTIVIDAD A
LEFT JOIN ESTRUCTURA_OPERACION O
ON A.IDACTIVIDAD = O.IDACTIVIDAD
WHERE O.IDSERVICIO = '{$idcontratodepto}'
AND O.IDCLIENTE = '{$idcliente}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Nivel Funcional
	function consultaNivelFuncioanl(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDNIVELFUNCIONAL, NOMBRE 'NIVEL'
FROM NIVELFUNCIONAL";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Tipo Contrato
	function consultaTipoContrato(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM TIPO_CONTRATO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Tipo licencia
	function consultaTipoLicencia(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM TIPO_LICENCIA";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Sucursl
	function consultaSucursal(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT S.IDSUCURSAL, S.SUCURSAL, A.COMUNA
FROM SUCURSAL S
LEFT JOIN AREAFUNCIONAL A
ON S.IDAREAFUNCIONAL  = A.IDAREAFUNCIONAL";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Sucursl
	function consultaSindicato(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM SINDICATO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select Tipo Jornada
	function consultaTipoJornada(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM TIPO_JORNADA";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select forma pago
	function consultaFormaPago(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM FORMA_PAGO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select forma pago
	function consultaBanco(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM BANCO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select forma AFP
	function consultaAFP(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDAFP, AFP
FROM AFP";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select forma AFP Porcentaje
	function consultaAFPPorcentaje($afp){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT TASA, SIS
FROM rriveros_generica.AFP
WHERE FECHA =
(
SELECT MAX(FECHA)
FROM rriveros_generica.AFP
)
AND AFP = '{$afp}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Select forma AFP
	function consultaSalud(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDSALUD, SALUD
FROM SALUD";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUsuarios(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT '' S, U.RUT, U.NOMBRE, U.EMAIL,U.ESTADO, P.NOMBRE 'PERFIL'
FROM USUARIO U, PERFIL P
WHERE U.IDPERFIL = P.IDPERFIL";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaPerfilTipos(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM PERFIL
ORDER BY NOMBRE";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
}

function chequeaEmail($email){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDUSUARIO
FROM USUARIO
WHERE EMAIL = '" . $email . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaUsuario($rutUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT IDUSUARIO
	FROM USUARIO
	WHERE RUT = '" . $rutUsuario . "'";
				if ($row = $con->query($sql)) {

					$array = $row->fetch_array(MYSQLI_BOTH);

					return $array;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function ingresaUsuario( $rutIngreso, $apellidos, $nombres, $email,$pass,$perfilUsuario){
				$con = conectar();
				$con->query("START TRANSACTION");
				if($con != 'No conectado'){
					$sql = "INSERT INTO USUARIO(NOMBRE,EMAIL,RUT,PASS,ESTADO,IDPERFIL)
			VALUES ( '" .$nombres ."',
			'" . $email . "', '" . $rutIngreso . "',
			'" . $pass . "',
			'Activo','" . $perfilUsuario . "')";
					if ($con->query($sql)) {
						$con->query("COMMIT");
						return "Ok";
					}
					else{
						return $con->error;
						$con->query("ROLLBACK");

					}
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
		}

		function desactivarUsuario($rutUsuario){
				$con = conectar();
				$con->query("START TRANSACTION");
				if($con != 'No conectado'){
					$sql = "UPDATE USUARIO
		SET ESTADO = 'Desactivado'
		WHERE RUT  = '" . $rutUsuario . "'";
					if ($con->query($sql)) {
						$con->query("COMMIT");
					  return "Ok";
					}
					else{
						return $con->error;
						$con->query("ROLLBACK");
					}
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}

			function editarUsuario( $rutEditar, $nombre, $email, $estado){
					$con = conectar();
					if($con != 'No conectado'){
						$sql = "UPDATE USUARIO
			SET RUT = '" . $rutEditar . "',
			NOMBRE = '" .$nombre . "',
			EMAIL = '" .$email . "',
			ESTADO = '" .$estado . "'
			WHERE RUT = '" . $rutEditar . "'";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					return $con->error;
					$con->query("ROLLBACK");
				}
				}
				else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

function datosOtDTH(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "CALL OTDTH_TODOS()";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaAreasFuncionales(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT IDAREAFUNCIONAL, COMUNA, CODIGOREGION
FROM AREAFUNCIONAL
WHERE OPERA = 1
ORDER BY CODIGOREGION ASC";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaAreasFuncionalesTabla(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT '' S, IDAREAFUNCIONAL, COMUNA, CODIGOREGION,
		CASE WHEN OPERA = 1
THEN '<span style=\"color: green;\" class=\"fas fa-check-double\"></span>&nbsp;&nbsp;Con operación'
ELSE '<span style=\"color: red;\" class=\"fas fa-times\"></span>&nbsp;&nbsp;Sin operación'
END OPERA,
CASE WHEN OPERA = 1
THEN 'Con operación'
ELSE 'Sin operación'
END 'OPERA_TEXTO'
FROM AREAFUNCIONAL
ORDER BY CODIGOREGION ASC";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function actualizaOperaAreaFuncional($idareafuncional){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE AREAFUNCIONAL
		SET OPERA = 1
		WHERE IDAREAFUNCIONAL = '{$idareafuncional}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function quitaOperaAreaFuncional($idareafuncional){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE AREAFUNCIONAL
		SET OPERA = 0
		WHERE IDAREAFUNCIONAL = '{$idareafuncional}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function borrarComunas($rut){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "DELETE A
FROM PERSONAL P
LEFT JOIN AREAFUNCIONAL_PERSONAL A
ON P.IDPERSONAL = A.IDPERSONAL
WHERE P.DNI = '{$rut}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function insertarComunaPersonal($rut, $idareafuncional){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "INSERT INTO AREAFUNCIONAL_PERSONAL(IDPERSONAL, IDAREAFUNCIONAL)
SELECT IDPERSONAL, '{$idareafuncional}'
FROM PERSONAL
WHERE DNI = '{$rut}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function imagenLogin($url){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "CALL rriveros_generica.LOGOLOGIN('{$url}')";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function consultaDespachos(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT P.IDPERSONAL, P.DNI, P.NOMBRES, P.APELLIDOS,
GROUP_CONCAT(A.COMUNA) 'COMUNA'
FROM PERSONAL P
LEFT JOIN AREAFUNCIONAL_PERSONAL AP
ON P.IDPERSONAL = AP.IDPERSONAL
LEFT JOIN AREAFUNCIONAL A
ON AP.IDAREAFUNCIONAL = A.IDAREAFUNCIONAL
WHERE P.CARGO LIKE '%Despacho%'
AND AP.IDPERSONAL IS NOT NULL
GROUP BY  P.IDPERSONAL, P.DNI, P.NOMBRES, P.APELLIDOS";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaTecnicos(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT P.IDPERSONAL, P.DNI, P.NOMBRES, P.APELLIDOS,
GROUP_CONCAT(A.COMUNA) 'COMUNA'
FROM PERSONAL P
LEFT JOIN AREAFUNCIONAL_PERSONAL AP
ON P.IDPERSONAL = AP.IDPERSONAL
LEFT JOIN AREAFUNCIONAL A
ON AP.IDAREAFUNCIONAL = A.IDAREAFUNCIONAL
WHERE P.CARGO LIKE '%tecnico%'
GROUP BY  P.IDPERSONAL, P.DNI, P.NOMBRES, P.APELLIDOS";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function actualizarDespachoOTDTH($folio, $idDespacho){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE SIAD_NOTIF_DTH
SET IDDESPACHO = '{$idDespacho}'
WHERE NATENCION  = '{$folio}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function datosOtDTHModificado($folios){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "CALL OTDTH('{$folios}',',')";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function actualizarTecnicoOTDTH($folio, $idTecnico){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE SIAD_NOTIF_DTH
SET IDTECNICO = '{$idTecnico}',
ESTADO_DESPACHO = 'Asignada'
WHERE NATENCION  = '{$folio}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function datosEstadoDTH(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT IDESTADO_DTH, ESTADO, MIGRACION
FROM ESTADO_DTH
WHERE ESTADO <> 'Liquidada'";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function actualizarEstadoOTDTH($folio, $estado){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE SIAD_NOTIF_DTH
SET ESTADO_DESPACHO = '{$estado}'
WHERE NATENCION  = '{$folio}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function datosRango(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT DISTINCT NOMBRE 'RANGO'
FROM RANGO";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function datosRegion(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT DISTINCT CODIGOREGION 'REGION'
FROM AREAFUNCIONAL A
WHERE OPERA = 1";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function datosComuna($region){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT COMUNA
FROM AREAFUNCIONAL A
WHERE OPERA = 1
AND CODIGOREGION = '{$region}'";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function actualizarAgendaOTDTH($folio, $cliente, $comuna, $numero, $calle, $fecha, $fono1, $fono2, $hora, $rango, $region, $rut){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE SIAD_NOTIF_DTH
SET CLIENTE = '{$cliente}',
COMUNA = '{$comuna}',
CALLE = '{$calle}',
NUMERO = '{$numero}',
FECHA_AGENDA = '{$fecha}',
FONO1 = '{$fono1}',
FONO2 = '{$fono2}',
HORA_AGENDA = '{$hora}',
RANGO = '{$rango}',
REGION = '{$region}',
RUT = '{$rut}'
WHERE NATENCION  = '{$folio}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function consultaPass($rut, $passNuevo){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PASS
FROM USUARIO
WHERE RUT = '" . $rut . "'
AND PASS = '" . $passNuevo . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}else{
                return $con->error;
            }
		}else{
			return "Error";
		}
	}

	function actualizaPass($rut, $pass){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE USUARIO
SET PASS = '" . $pass . "'
WHERE RUT  = '" . $rut . "'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}

function consultaDTHLineaTiempo($fecha){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT initCap(CONCAT(P.NOMBRES, ' ', P.APELLIDOS)) 'NOMBRE',
CONCAT(initCap(S.TIPO_TICKET),' - ', 'DTH') 'TIPO_TICKET', S.ESTADO_DESPACHO, E.COLOR,
S.HORA_AGENDA 'INICIO',
HOUR(S.HORA_AGENDA) 'HORA_INICIO',
MINUTE(S.HORA_AGENDA) 'MIN_INICIO',
SECOND(S.HORA_AGENDA) 'SEC_INICIO',
ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00')) 'TERMINO',
HOUR(ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00'))) 'HORA_FIN',
MINUTE(ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00'))) 'MIN_FIN',
SECOND(ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00'))) 'SEC_FIN',
S.NATENCION 'FOLIO'
FROM SIAD_NOTIF_DTH S
INNER JOIN PERSONAL P
ON S.IDTECNICO  = P.IDPERSONAL
LEFT JOIN ESTADO_DTH E
ON S.ESTADO_DESPACHO = E.ESTADO
LEFT JOIN TIEMPO_DTH T
ON S.TIPO_TICKET = T.TIPO_TICKET
WHERE S.FECHA_AGENDA = '{$fecha}'
AND S.`ESTADO ORDEN` <> 'PENDIENTE COMERCIAL'
AND S.`ESTADO ORDEN` <> 'RECHAZO'";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaDTHLineaTiempoTac($fecha, $rut){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT initCap(CONCAT(P.NOMBRES, ' ', P.APELLIDOS)) 'NOMBRE',
CONCAT(initCap(S.TIPO_TICKET),' - ', 'DTH') 'TIPO_TICKET', S.ESTADO_DESPACHO, E.COLOR,
S.HORA_AGENDA 'INICIO',
HOUR(S.HORA_AGENDA) 'HORA_INICIO',
MINUTE(S.HORA_AGENDA) 'MIN_INICIO',
SECOND(S.HORA_AGENDA) 'SEC_INICIO',
ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00')) 'TERMINO',
HOUR(ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00'))) 'HORA_FIN',
MINUTE(ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00'))) 'MIN_FIN',
SECOND(ADDTIME(S.HORA_AGENDA, CONCAT(TRUNCATE(T.TIEMPO/60,0), ':',60*(T.TIEMPO/60 - TRUNCATE(T.TIEMPO/60,0)),':00'))) 'SEC_FIN',
S.NATENCION 'FOLIO',
initCap(S.CALLE) 'CALLE', S.COMUNA, S.NUMERO
FROM SIAD_NOTIF_DTH S
INNER JOIN PERSONAL P
ON S.IDTECNICO  = P.IDPERSONAL
LEFT JOIN ESTADO_DTH E
ON S.ESTADO_DESPACHO = E.ESTADO
LEFT JOIN TIEMPO_DTH T
ON S.TIPO_TICKET = T.TIPO_TICKET
WHERE S.FECHA_AGENDA = '{$fecha}'
AND S.IDTECNICO =
(
SELECT IDPERSONAL
FROM PERSONAL
WHERE DNI = '{$rut}'
)
AND S.`ESTADO ORDEN` <> 'PENDIENTE COMERCIAL'
AND S.`ESTADO ORDEN` <> 'RECHAZO'
ORDER BY S.HORA_AGENDA ASC
";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaTipoOTDTH($tipoTk){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT DISTINCT TIPO
FROM SIAD_NOTIF_DTH_TIPO
WHERE TIPO_TICKET = '{$tipoTk}'";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaTipoTkOTDTH(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT DISTINCT TIPO_TICKET
FROM SIAD_NOTIF_DTH_TIPO";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function actualizaObservacionDTH($folio, $observacion){
	$con = conectar();
	$con->query("START TRANSACTION");
	if($con != 'No conectado'){
		$sql = "UPDATE SIAD_NOTIF_DTH
SET OBSERVACION_DESPACHO = '{$observacion}'
WHERE NATENCION = '{$folio}'";
		if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	else{
		$con->query("ROLLBACK");
		return "Error";
	}
}
?>
