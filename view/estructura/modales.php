<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>

<!-- Modal de splash -->
<div id="modalAlertasSplash" class="modal modal-fullscreen fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog" role="document">

    <!-- Modal content-->
    <div class="modal-content-t">
      <div class="modal-body alerta-modal-body">
        <h4 id="textoModalSplash"></h4>
        <button id="buttonAceptarAlertaSplash" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPassSplash" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal de alertas -->
<div id="modalAlertas" class="modal modal-fullscreen fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog modal-dialog-box" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body alerta-modal-body">
        <h6 id="textoModal"></h6>
        <button id="buttonAceptarAlerta" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPass" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal ingreso de personal interno -->
<div id="modalIngresoPersonalInterno" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-dialog-box modal-xl" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 style="color:gray;" id="tituloIngresoPersonalInterno"><span class="fas fa-user-plus"></span>&nbsp;&nbsp;Ingreso personal interno</h5>
      </div>
      <div id="bodyIngresoPersonalInterno" class="modal-body alerta-modal-body" style="overflow-y: scroll;">
        <div class="row" style="text-align: left; margin-bottom: 20pt;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos básicos</label>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">RUN</label>
            <input id="rutIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Apellidos</label>
            <input id="apellidosIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Nombres</label>
            <input id="nombresIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">F. nacimiento</label>
            <input id="fechaNacIngresoPersonalInterno" class="form-control" type="date" value="">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Sexo</label>
						<select id="sexoIngresoPersonalInterno" class="form-control">
							<option value="Mujer">Mujer</option>
							<option value="Hombre">Hombre</option>
            </select>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Estado civil</label>
            <select id="estadoCivilIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Situacion militar</label>
            <select id="sitMilitarIngresoPersonalInterno" class="form-control">
              <option value="Pendiente">Pendiente</option>
              <option value="Al día">Al día</option>
            </select>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Fono</label>
            <input id="telefonoIngresoPersonalInterno" class="form-control input-number" type="text" value="">
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Fono emergencia</label>
            <input id="telefonoEmergenciaIngresoPersonalInterno" class="form-control input-number" type="text" value="">
          </div>
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Contacto emergencia</label>
            <input id="contactoIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Nacionalidad</label>
            <select id="nacionalidadIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Región</label>
            <select disabled id="regionIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Provincia</label>
            <select disabled id="provinciaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Comuna</label>
            <select id="comunaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Dirección</label>
            <input id="direccionIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos educacionales</label>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Nivel</label>
            <select id="nivelEduIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Profesión</label>
            <input id="profesionIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Especialidad</label>
            <input id="especialidadIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos laborales</label>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Centro costo</label>
            <select id="cecoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Servicio o Depto.</label>
            <select id="contratoDeptoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cliente</label>
            <select id="clienteIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Actividad</label>
            <select id="actividadIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Fecha ingreso</label>
            <input id="fechaIngIngresoPersonalInterno" class="form-control" type="date" value="">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Mano de obra</label>
            <select id="clasificacionIngresoPersonalInterno" class="form-control">
							<option value="MOD">MOD</option>
							<option value="MOI">MOI</option>
							<option value="MOE">MOE</option>
            </select>
          </div>
					<div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Cargo</label>
            <input id="cargoIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Nivel funcional</label>
            <select id="nivelFuncionalIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Tipo contrato</label>
            <select id="tipoContratoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Requiere uniforme</label>
            <select id="requiereUniformeIngresoPersonalInterno" class="form-control">
							<option value="SI">Si</option>
							<option value="NO">No</option>
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Talla uniforme</label>
            <input id="tallaIngresoPersonalInterno" class="form-control" type="number" value="30" min="34" max="60">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Posee licencia</label>
            <select id="poseeLicenciaIngresoPersonalInterno" class="form-control">
							<option value="SI">Si</option>
							<option value="NO">No</option>
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Tipo licencia</label>
            <select id="tipoLicenciaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Sucursal</label>
            <select id="sucursalIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Sindicato</label>
            <select id="sindicatoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Tipo jornada</label>
            <select id="tipoJornadaIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos salariales</label>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Sueldo base (bruto)</label>
            <input id="sueldoBaseIngresoPersonalInterno" class="form-control input-money" type="text" value="$ 0">
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Forma pago</label>
						<select id="formaPagoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Banco</label>
						<select id="bancoPagoIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cuenta banco</label>
            <input id="cuentaBancoIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
            <hr class="hr-separador">
          </div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos previsionales</label>
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">AFP</label>
						<select id="afpIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">(%) AFP</label>
            <input disabled id="afpPorcentajeIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">(%) SIS</label>
            <input disabled id="sisPorcentajeIngresoPersonalInterno" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Salud</label>
						<select id="saludIngresoPersonalInterno" class="form-control">
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cargas fam.</label>
						<select id="cargasIngresoPersonalInterno" class="form-control">
							<option value="SI">Si</option>
							<option value="NO">No</option>
            </select>
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Cant. Cargas</label>
            <input id="cantidadCargasIngresoPersonalInterno" class="form-control" type="number" value="0" min="0">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Plan APV</label>
            <input id="planAPVIngresoPersonalInterno" class="form-control" type="text">
          </div>
					<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
						<label style="font-weight: bold;">Monto APV</label>
            <input id="montoAPVIngresoPersonalInterno" class="form-control input-money" type="text" value="$ 0">
          </div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button id="guardarIngresoPersonalInterno" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
        <button id="cancelarIngresoPersonalInterno" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div id="modalIngresoUsuario" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-dialog-box modal-lg" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 style="color:gray;" id="tituloIngresoUsuario"><span class="fas fa-user-plus"></span>&nbsp;&nbsp;Ingreso usuario</h5>
      </div>
      <div id="bodyIngresoUsuario" class="modal-body alerta-modal-body">
        <div class="row" style="text-align: left; margin-bottom: 20pt;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos</label>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">RUN</label>
            <input id="rutIngresoUsuario" class="form-control" type="text" value=" ">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Apellidos</label>
            <input id="apellidosIngresoUsuario" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Nombres</label>
            <input id="nombresIngresoUsuario" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Email</label>
            <input id="emailUsuario" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Perfil</label>
						<select id="perfilUsuario" class="form-control">
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button id="guardarIngresoUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
        <button id="cancelarIngresoUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div id="modalDesactivarUsuario" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-dialog-box modal-lg" role="document">

    <!-- Modal content-->
    <div class="modal-content">

			<div class="modal-header">
        <h5 style="color:gray;" id="tituloDesactivarUsuario"><span class="fas fa-ban"></span>&nbsp;&nbsp;Desactivar usuario</h5>
      </div>
			<div id="bodyDesactivarUsuario" class="modal-body alerta-modal-body">
        <div class="row" style="text-align: left;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;"></label>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <font style="font-weight: bold; font-size: 11pt;">¿Está seguro que desea desactivar al usuario?</font>
            <font style="font-weight: bold; font-size: 11pt;">¿Está seguro que desea desactivar al usuario?</br><i style="font-weight: normal; font-size: 11pt;" id="textoDesactivarUsuario"></i></font>
          </div>

        </div>
      </div>
			<div class="modal-footer" style="text-align: left;">
        <button id="guardarDesactivarUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
        <button id="cancelarDesactivarUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div id="modalEditarUsuario" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-dialog-box modal-lg" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 style="color:gray;" id="tituloIngresoUsuario"><span class="fas fa-user-edit"></span>&nbsp;&nbsp;Editar usuario</h5>
      </div>
      <div id="bodyEditarUsuario" class="modal-body alerta-modal-body">
        <div class="row" style="text-align: left; margin-bottom: 20pt;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;">Datos</label>
          </div>
          <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">RUN</label>
            <input disabled id="rutEditarUsuario" class="form-control" type="text" value=" ">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Nombre completo</label>
            <input id="nombreEditarUsuario" class="form-control" type="text" value="">
          </div>
          <div class="col-xl-5 col-lg-5 col-md-8 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Email</label>
            <input id="emailEditarUsuario" class="form-control" type="text" value="">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
            <label style="font-weight: bold;">Perfil</label>
						<input disabled id="perfilEditarUsuario" class="form-control">
          </div>
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 input-group-sm"  style="margin-top: 10pt;">
						<label style="font-weight: bold;">Estado</label>
						<select id="estadoEditarUsuario" class="form-control">
							<option value="Activo">Activo</option>
							<option value="Desactivado">Desactivado</option>
						</select>
					</div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: left;">
        <button id="guardarEditarUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
        <button id="cancelarEditarUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div id="modalMapaOT" class="modal modal-fullscreen fade" role="dialog" style="z-index: 2500;">
    <div class="modal-dialog modal-dialog-box modal-xl" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:gray;"><span class="fas fa-map-marked-alt">&nbsp;&nbsp;
									<span id="tituloMapaOT"></span>
								</h5>
            </div>
            <div id="bodyMapaOT" class="modal-body alerta-modal-body" style="overflow-y: scroll;">
                <input type="hidden" id="idmodalMapaOT">
                <div id="mapaOTGoogle" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                </div>
            </div>
            <div class="modal-footer" style="text-align: left;">
                <button id="cerrarModalMapaOT" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalAsignarComuna" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-lg" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:gray;"><span class="fas fa-map-marked-alt"></span>&nbsp;&nbsp;
									<span>Asignar comuna:</span>
									<span id="tituloAsignarComuna" style="font-size: 12pt;"></span>
								</h5>
            </div>
            <div id="bodyAsignarComuna" class="modal-body alerta-modal-body" style="text-align: left;">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm">
		            <label style="font-weight: bold;">Comunas</label>
								<select id="comunaAsignarComuna" class="form-control" multiple>

		            </select>
		          </div>
            </div>
            <div class="modal-footer" style="text-align: left;">
								<button id="guardarAsignarComuna" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
                <button id="cerrarAsignarComuna" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalAsignarDespachoOTDTH" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-lg" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:gray;"><span class="fas fa-headset"></span>&nbsp;&nbsp;
									<span>Asignar despacho</span>
									<br>
									<span id="tituloAsignarDespachoOTDTH" style="font-size: 12pt;"></span>
								</h5>
            </div>
            <div id="bodyAsignarDespachoOTDTH" class="modal-body alerta-modal-body" style="text-align: left;">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm">
		            <label style="font-weight: bold;">Despacho</label>
								<select id="despachoAsignarDespachoOTDTH" class="form-control">

		            </select>
		          </div>
            </div>
            <div class="modal-footer" style="text-align: left;">
								<button id="guardarAsignarDespachoOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
                <button id="cerrarAsignarDespachoOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalAsignarTecnicoOTDTH" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-lg" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:gray;"><span class="fas fa-user-alt"></span>&nbsp;&nbsp;
									<span>Asignar técnico</span>
									<br>
									<span id="tituloAsignarTecnicoOTDTH" style="font-size: 12pt;"></span>
								</h5>
            </div>
            <div id="bodyAsignarTecnicoOTDTH" class="modal-body alerta-modal-body" style="text-align: left;">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm">
		            <label style="font-weight: bold;">Técnico</label>
								<select id="despachoAsignarTecnicoOTDTH" class="form-control">

		            </select>
		          </div>
            </div>
            <div class="modal-footer" style="text-align: left;">
								<button id="guardarAsignarTecnicoOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
                <button id="cerrarAsignarTecnicoOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalAsignarEstadoOTDTH" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-md" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:gray;"><span class="far fa-edit"></span>&nbsp;&nbsp;
									<span>Asignar estado</span>
									<br>
									<span id="tituloAsignarEstadoOTDTH" style="font-size: 12pt;"></span>
								</h5>
            </div>
            <div id="bodyAsignarEstadoOTDTH" class="modal-body alerta-modal-body" style="text-align: left;">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm">
		            <label style="font-weight: bold;">Estado</label>
								<select id="despachoAsignarEstadoOTDTH" class="form-control">

		            </select>
		          </div>
            </div>
            <div class="modal-footer" style="text-align: left;">
								<button id="guardarAsignarEstadoOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
                <button id="cerrarAsignarEstadoOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalAsignarAgendarOTDTH" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-lg" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:gray;"><span class="far fa-calendar-check"></span>&nbsp;&nbsp;
									<span>Agendar OT DTH</span>
									<br>
									<span id="tituloAsignarAgendarOTDTH" style="font-size: 12pt;"></span>
								</h5>
            </div>
            <div id="bodyAsignarAgendarOTDTH" class="modal-body alerta-modal-body" style="text-align: left; overflow-y: scroll;">
							<div class="row">
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Fecha agenda</label>
			            <input id="fechaCompromisoAgendarOTDTH" class="form-control" type="text" value="" onfocus="blur();">
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
			            <label style="font-weight: bold;">Rango</label>
									<select id="rangoAgendarOTDTH" class="form-control">

			            </select>
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Rut Cliente</label>
			            <input disabled id="rutClienteAgendarOTDTH" class="form-control" type="text" value="">
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Cliente</label>
			            <input disabled id="clienteAgendarOTDTH" class="form-control" type="text" value="">
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
			            <label style="font-weight: bold;">Region</label>
									<select disabled id="regionAgendarOTDTH" class="form-control">

			            </select>
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
			            <label style="font-weight: bold;">Comuna</label>
									<select disabled  id="comunaAgendarOTDTH" class="form-control">

			            </select>
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Calle</label>
			            <input disabled id="calleAgendarOTDTH" class="form-control" type="text" value="">
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Número</label>
			            <input disabled id="numeroAgendarOTDTH" class="form-control" type="text" value="">
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Fono 1</label>
			            <input id="fono1AgendarOTDTH" class="form-control" type="text" value="">
			          </div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
									<label style="font-weight: bold;">Fono 2</label>
			            <input id="fono2AgendarOTDTH" class="form-control" type="text" value="">
			          </div>
							</div>
            </div>
            <div class="modal-footer" style="text-align: left;">
								<button id="guardarAsignarAgendarOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
                <button id="cerrarAsignarAgendarOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalResetPassUsuario" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-dialog-box modal-lg" role="document">

    <!-- Modal content-->
    <div class="modal-content">
			<div class="modal-header">
        <h5 style="color:gray;" id="tituloResetPassUsuario"><span class="fas fa-user-lock"></span>&nbsp;&nbsp;Resetear Contraseña usuario</h5>
      </div>
			<div id="bodyResetPassUsuario" class="modal-body alerta-modal-body">
        <div class="row" style="text-align: left; margin-bottom: 20pt;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label style="font-weight: bold; color: gray; font-size: 14pt;"></label>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <font style="font-weight: bold; font-size: 11pt;">¿Está seguro que desea resetear la contraseña del usuario?</br><i style="font-weight: normal; font-size: 11pt;" id="tituloDesvincular"></i></font>
          </div>

        </div>
      </div>
			<div class="modal-footer" style="text-align: left;">
        <button id="guardarResetPassUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
        <button id="cancelarResetPassUsuario" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal observacion DTH-->
<div id="modalObsDTH" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-lg" role="document">

    <!-- Modal content-->
		<div class="modal-content">
				<div class="modal-header">
						<h5 style="color:gray;"><span class="fas fa-pencil-alt"></span>&nbsp;&nbsp;
							<span>Ingresar observación</span>
							<br>
							<span id="tituloIngrObsDTH" style="font-size: 12pt;"></span>
						</h5>
				</div>
				<div id="bodyObsDTH" class="modal-body alerta-modal-body" style="">
					<label style="color: red; margin-left: 10px; display: none; font-size: 12px;">Observación</label>
					<div class="row" style="text-align: left; margin-bottom: 20pt;">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<textarea id="observacionDTHIng" class="form-control" rows="10"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="text-align: left;">
	      <!-- Botones -->
				<button id="guardarObsDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
				<button id="cancelarObsDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	  		</div>
  	</div>
	</div>
</div>

<!-- Modal ingresar OT DTH-->
<div id="modalIngOTDTH" class="modal modal-fullscreen fade" role="dialog">
    <div class="modal-dialog modal-dialog-box modal-lg" role="document">

    <!-- Modal content-->
		<div class="modal-content">
				<div class="modal-header">
						<h5 style="color:gray;"><span class="far fa-plus-square"></span>&nbsp;&nbsp;
							<span>Ingresar OT Manual</span>
							<br>
							<span id="tituloIngOTDTH" style="font-size: 12pt;"></span>
						</h5>
				</div>
				<div id="bodyIngOTDTH" class="modal-body alerta-modal-body" style="overflow-y: scroll;">
					<label style="color: red; margin-left: 10px; display: none; font-size: 12px;">Observación</label>
					<div class="row" style="text-align: left; margin-bottom: 20pt;">
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Folio (10 digitos)</label>
	            <input id="folioIngOTDTH" class="form-control" type="text" value="">
						</div>
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Fecha/Hora agenda</label>
	            <input id="fechaHoraIngOTDTH" class="form-control" type="text" value="" onfocus="blur();">
						</div>
						<div class="col-xl-12 col-lg-21 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Despacho</label>
							<select id="despachoIngOTDTH" class="form-control">
	            </select>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Contrato</label>
	            <input id="contratoIngOTDTH" class="form-control" type="text" value="">
						</div>
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Valor contrato</label>
	            <input id="valorContratoIngOTDTH" class="form-control" type="text" value="">
						</div>
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 d-none d-sm-none d-md-block input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">&nbsp;</label>
							<label style="font-weight: bold;">&nbsp;</label>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">RUN/RUT</label>
	            <input id="rutIngOTDTH" class="form-control" type="text" value="">
						</div>
						<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 d-none d-sm-none d-md-block input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">&nbsp;</label>
							<label style="font-weight: bold;">&nbsp;</label>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Nombre/Razón social</label>
	            <input id="razonSocialIngOTDTH" class="form-control" type="text" value="">
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Dirección</label>
	            <input id="direccionIngOTDTH" class="form-control" type="text" value="">
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Comuna - Región</label>
							<select id="comunaRegionIngOTDTH" class="form-control">
	            </select>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 d-none d-sm-none d-md-block input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">&nbsp;</label>
							<label style="font-weight: bold;">&nbsp;</label>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Tipo Ticket</label>
							<select id="tipoTkIngOTDTH" class="form-control">
	            </select>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 input-group-sm" style="margin-top: 10pt;">
							<label style="font-weight: bold;">Tipo</label>
							<select id="tipoIngOTDTH" class="form-control">
	            </select>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="text-align: left;">
	      <!-- Botones -->
				<button id="guardarIngOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary">Guardar</button>
				<button id="cancelarIngOTDTH" style="margin-top: 10px; display: block;" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	  		</div>
  	</div>
	</div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCb15rFHKSkFxmZbQIo6KVes2-GR3N-LcQ&libraries=places&callback=initMap" async defer>
</script>
<script type="text/javascript">
	function initMap() {
		var input = document.getElementById('direccionIngOTDTH');
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.addListener('place_changed', function() {
			 var place = autocomplete.getPlace();
			 // document.getElementById('location-snap').innerHTML = place.formatted_address;
			 // document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
			 // document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
		 });
	 }
	 $("#direccionIngOTDTH").attr("placeholder","");
</script>
