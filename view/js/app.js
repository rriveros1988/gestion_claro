/* Funciones de apoyo */
function esconderMenu(){
  $("#logoLinkWeb").hide();
  $("#menu-lateral").css("width","45px");
  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
  $("#logoMenu").css("color","black");
  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
  if($("#sesionActiva").val() == 1){
    $("#lineaMenu").fadeOut();
    $(".contenedor-logos").css("display","none");
    $(".contenedor-logos").find('li').css("display","none");
  }
  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
  $("#logoMenu").fadeIn();
}

function mensajeWssp(){
  setTimeout(function(){
    $('#modalAlertasSplash').modal('hide');
    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $('#btnWssp').css("opacity",1);
      $('#btnWssp').fadeIn();
      setTimeout(function(){
        $('#bocadilloWssp').fadeIn();
      },1000);
      setTimeout(function(){
        $('#bocadilloWssp').fadeOut();
      },4000);
      setTimeout(function(){
        $('#btnWssp').css("opacity",0.2);
      },5000);
    }
  },1000);
}

function mensajeWsspSinTexto(){
  setTimeout(function(){
    $('#modalAlertasSplash').modal('hide');
    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $('#btnWssp').css("opacity",0.2);
      $('#btnWssp').fadeIn();
    }
  },1000);
}

var lineaTiempo = '';

var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/asignadas", {
        controller: "lineaController",
        controllerAs: "vm",
        templateUrl : "view/ot/asignadas.html"
    })
    .when("/logout", {
        controller: "logoutController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/otDTH", {
        controller: "otDTHController",
        controllerAs: "vm",
        templateUrl : "view/gestionDTH/otDTH.html"
    })
    .when("/personal", {
        controller: "personalController",
        controllerAs: "vm",
        templateUrl : "view/adminPersonal/personal.html"
    })
    .when("/mantenedorAreaFun", {
        controller: "mantenedorAreaFunController",
        controllerAs: "vm",
        templateUrl : "view/adminPersonal/areasFuncional.html"
    })
    .when("/usuarios", {
        controller: "usuariosController",
        controllerAs: "vm",
        templateUrl : "view/usuario/usuarios.html"
    })
    .when("/asignadasTac", {
        controller: "lineaTacController",
        controllerAs: "vm",
        templateUrl : "view/ot/asignadasTac.html"
    })
    .when("/changePass", {
        controller: "changePassController",
        controllerAs: "vm",
        templateUrl : "view/home/changePass.html"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("changePassController", function(){
    clearInterval(lineaTiempo);
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);
    setTimeout(async function(){
        $('#contenido').fadeIn();
        $('#menu-lateral').fadeIn();
        $('#footer').fadeIn();

        $("#guardarChangePass").css("width",$("#passNuevoConfirmar").width()+100);

        addCambiaPass();

        await esconderMenu();
        mensajeWsspSinTexto();
    },1500);
});

app.controller("homeController", function(){
    clearInterval(lineaTiempo);
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);
    setTimeout(async function(){
        $('#contenido').fadeIn();
        $('#menu-lateral').fadeIn();
        $('#footer').fadeIn();
        var url = window.location.origin;

        $("#imgLlave").attr("src","controller/cargarLogo.php?url=" + url);

        await esconderMenu();
        $("#logoMenu").fadeOut();
        // $('#menu-lateral').hover(function(){
        //     $("#menu-lateral").css("width","200px");
        // },
        // function() {
        //     $("#menu-lateral").css("width","45px");
        // });
        $("#loginSystem").show("slide", {direction: "up"}, 800);
    },1500);
    setTimeout(function(){
      mensajeWssp();
    },2000);
});

app.controller("lineaController", function(){
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);

    setTimeout(async function(){
      $('#contenido').fadeIn();
      $('#menu-lateral').fadeIn();
      $('#footer').fadeIn();

      w = $(document).width();
      h = $(document).height() - 170;

      $("#timeline").css("height", h);

      var hoy = moment().format('YYYY-MM-DD');
      $("#fechaLina").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        // minDate: min,
        // maxDate: min,
        yearRange: '1920:2040',
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
      });
      $("#fechaLina").val(hoy.toString());

      dibujaTimeline($("#fechaLina").val());

      lineaTiempo = setInterval(function (e) {
        dibujaTimeline($("#fechaLina").val());
        e.preventDefault();
        e.stopImmediatePropagation();
      }, 60000);

      var element = document.getElementById('contenido');
      new ResizeSensor(element, function() {
          dibujaTimeline($("#fechaLina").val());
      });

      await esconderMenu();
      mensajeWsspSinTexto();
    },1500);
});

app.controller("logoutController", function(){
  clearInterval(lineaTiempo);
  setTimeout(function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
  },100);
  $.ajax({
      url:   'controller/cerraSesion.php',
      type:  'post',
      success: function (response) {
        $(".contenedor-logos").css("display","none");
        $(".contenedor-logos").find('li').css("display","none");
        $("#sesionActiva").val("0");
        $("#sesionActivaUso").val("0");
        $("#logoLinkWeb").fadeOut();
        $("#logoMenu").fadeOut();
        $("#lineaMenu").fadeOut();
        $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
        $("#menu-lateral").css("width","45px");
        $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
        $("#logoMenu").css("color","black");
        $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
        $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
        $("#DivPrincipalMenu").empty();

        window.location.href = "#/home";
      }
  });
});

app.controller("otDTHController", function(){
    clearInterval(lineaTiempo);
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);
    setTimeout(async function(){
      var largo = Math.trunc(($(window).height() - ($(window).height()/100)*50)/30);
      await $('#tablaOtDTH').DataTable( {
          ajax: {
              url: "controller/datosOtDTH.php",
              type: 'POST'
          },
          columns: [
              { data: 'S'},
              { data: 'FOLIO', className: "negritaDataTable" } ,
              { data: 'ORIGEN' },
              { data: 'TIPO_TICKET' },
              { data: 'ESTADO_MANDANTE' },
              { data: 'ESTADO_DESPACHO' },
              { data: 'FECHA_CREACION'},
              { data: 'FECHA_AGENDA'},
              { data: 'HORA_AGENDA'},
              { data: 'RANGO', className: "centerDataTable"},
              { data: 'DIAS', className: "centerDataTable"},
              { data: 'DIAS_AGENDA', className: "centerDataTable"},
              { data: 'CANTIDAD_REAGENDAMIENTOS', className: "centerDataTable"},
              { data: 'REGION' , className: "centerDataTable"},
              { data: 'COMUNA' },
              { data: 'DIRECCION' },
              { data: 'RUT' },
              { data: 'CLIENTE' },
              { data: 'FONO1' },
              { data: 'FONO2' },
              { data: 'DESPACHO' },
              { data: 'TECNICO' },
              { data: 'OBSERVACION_DESPACHO', className: "centerDataTable"},
              { data: 'REFERENCIA' , className: "centerDataTable"},
              { data: 'REFERENCIA2'},
              { data: 'OBSERVACION_DESPACHO2'},
              { data: 'HORA_AGENDA'},
              { data: 'CALLE'},
              { data: 'NUMERO'},
              { data: 'SEMAFORO'}
          ],
          // rowReorder: {
          //   selector: 'td:nth-child(2)'
          // },
          // responsive: true,
          buttons: [
            {
                extend: 'excel',
                title: null,
                text: '<span class="far fa-file-excel"></span>&nbsp;&nbsp;Excel'
            },
            {
              text: '<span class="fas fa-broom"></span>&nbsp;&nbsp;Deseleccionar todo',
              action: function ( e, dt, node, config ) {
                var table = $('#tablaOtDTH').DataTable();
                $("#agendarOTDTH").attr("disabled","disabled");
        				$("#asignarDespachoOTDTH").attr("disabled","disabled");
        				$("#asignarTecnicoOTDTH").attr("disabled","disabled");
        				$("#estadoOTDTH").attr("disabled","disabled");
                table.rows().deselect();
              }
            }
          ],
          fixedColumns:   {
             leftColumns: 2
          },
          "columnDefs": [
            {
              "orderable": false,
              "className": 'select-checkbox',
              "targets": [ 0 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 24 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 25 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 26 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 27 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 28 ]
            },
            {
              "visible": false,
              "searchable": true,
              "targets": [ 29 ]
            }
          ],
          "select": {
              style: 'multi'
          },
          "scrollX": true,
          "paging": true,
          "ordering": true,
          "scrollCollapse": true,
          "order": [[ 7, "desc" ]],
          "info":     true,
          "lengthMenu": [[largo], [largo]],
          "dom": 'Bfrtip',
          "language": {
              "zeroRecords": "No tiene personal bajo su cargo",
              "info": "Registro _START_ de _END_ de _TOTAL_",
              "infoEmpty": "No tiene personal bajo su cargo",
              "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente"
              },
              "search": "Buscar: ",
              "select": {
                  "rows": "- %d registros seleccionados"
              },
              "infoFiltered": "(Filtrado de _MAX_ registros)"
          },
          "destroy": true,
          "autoWidth": false,
          "initComplete": function( settings, json){
            var table = $('#tablaOtDTH').DataTable();

            var cuerpoRegionDTH = '';
            cuerpoRegionDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(13).data().unique().length; i++){
              if(table.column(13).data().unique()[i] !== null){
                cuerpoRegionDTH += '<option value="' + table.column(13).data().unique()[i] + '">' + table.column(13).data().unique()[i] + '</option>';
              }
            }
            $("#regionOtDTH").html(cuerpoRegionDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#regionOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoComunaDTH = '';
            cuerpoComunaDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(14).data().unique().length; i++){
              if(table.column(14).data().unique()[i] !== null){
                cuerpoComunaDTH += '<option value="' + table.column(14).data().unique()[i] + '">' + table.column(14).data().unique()[i] + '</option>';
              }
            }
            $("#comunaOtDTH").html(cuerpoComunaDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#comunaOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoOrigenDTH = '';
            cuerpoOrigenDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(2).data().unique().length; i++){
              if(table.column(2).data().unique()[i] !== null){
                cuerpoOrigenDTH += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
              }
            }
            $("#origenOtDTH").html(cuerpoOrigenDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#origenOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoTTicketDTH = '';
            cuerpoTTicketDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(3).data().unique().length; i++){
              if(table.column(3).data().unique()[i] !== null){
                cuerpoTTicketDTH += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
              }
            }
            $("#tticketOtDTH").html(cuerpoTTicketDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#tticketOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpofechaCompDTH = '';
            cuerpofechaCompDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(7).data().unique().length; i++){
              if(table.column(7).data().unique()[i] !== null){
                cuerpofechaCompDTH += '<option value="' + table.column(7).data().unique()[i] + '">' + table.column(7).data().unique()[i] + '</option>';
              }
            }
            $("#fechaCompOtDTH").html(cuerpofechaCompDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#fechaCompOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoRangoDTH = '';
            cuerpoRangoDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(9).data().unique().length; i++){
              if(table.column(9).data().unique()[i] !== null){
                cuerpoRangoDTH += '<option value="' + table.column(9).data().unique()[i] + '">' + table.column(9).data().unique()[i] + '</option>';
              }
            }
            $("#rangoOtDTH").html(cuerpoRangoDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#rangoOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoOtDTH = '';
            cuerpoOtDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(20).data().unique().length; i++){
              if(table.column(20).data().unique()[i] !== null){
                cuerpoOtDTH += '<option value="' + table.column(20).data().unique()[i] + '">' + table.column(20).data().unique()[i] + '</option>';
              }
            }
            $("#despachoOtDTH").html(cuerpoOtDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#despachoOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoEstadoDTH = '';
            cuerpoEstadoDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(5).data().unique().length; i++){
              if(table.column(5).data().unique()[i] !== null){
                cuerpoEstadoDTH += '<option value="' + table.column(5).data().unique()[i] + '">' + table.column(5).data().unique()[i] + '</option>';
              }
            }
            $("#estadoOtDTH").html(cuerpoEstadoDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#estadoOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoObsDTH = '';
            cuerpoObsDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(29).data().unique().length; i++){
              if(table.column(29).data().unique()[i] !== null){
                cuerpoObsDTH += '<option value="' + table.column(29).data().unique()[i] + '">' + table.column(29).data().unique()[i] + '</option>';
              }
            }
            $("#semafotoOtDTH").html(cuerpoObsDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#semafotoOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoAgenDTH = '';
            cuerpoAgenDTH += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(12).data().unique().length; i++){
              if(table.column(12).data().unique()[i] !== null){
                cuerpoAgenDTH += '<option value="' + table.column(12).data().unique()[i] + '">' + table.column(12).data().unique()[i] + '</option>';
              }
            }
            $("#reagendamientoOtDTH").html(cuerpoAgenDTH);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#reagendamientoOtDTH").select2({
                  theme: "bootstrap"
              });
            }

            setTimeout(async function(){
              $('#contenido').fadeIn();
              $('#menu-lateral').fadeIn();
              $('#footer').fadeIn();
              $('#tablaOtDTH').DataTable().columns.adjust();
              await esconderMenu();
              await mensajeWsspSinTexto();
            },500);
          }
      });
    },1500);
});

app.controller("personalController", function(){
    clearInterval(lineaTiempo);
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);

    setTimeout(async function(){
      var largo = Math.trunc(($(window).height() - ($(window).height()/100)*50)/30);
      await $('#tablaPersonal').DataTable( {
          ajax: {
              url: "controller/datosPersonal.php",
              type: 'POST'
          },
          columns: [
              { data: 'S' },
              { data: 'DNI', className: "pointerDatatable" } ,
              { data: 'NOMBRE', className: "pointerDatatable" },
              { data: 'CARGO', className: "pointerDatatable" },
              { data: 'CLASIFICACION', className: "pointerDatatable" },
              { data: 'EMPRESA', className: "pointerDatatable" },
              { data: 'COMUNA', className: "pointerDatatable" },
              { data: 'IDCOMUNA', className: "pointerDatatable" }
          ],
          // responsive: true,
          buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 1,2,3,4,5,6 ]
                },
                title: null,
                text: '<span class="far fa-file-excel"></span>&nbsp;&nbsp;Excel'
            },
            {
              text: '<span class="fas fa-broom"></span>&nbsp;&nbsp;Deseleccionar todo',
              action: function ( e, dt, node, config ) {
                var table = $('#tablaPersonal').DataTable();
                $("#editarPersonal").attr("disabled","disabled");
        				$("#comunaPersonal").attr("disabled","disabled");
                table.rows().deselect();
              }
            }
          ],
          "columnDefs": [
            {
              "width": "5px",
              "targets": 0
            },
            {
              "orderable": false,
              "className": 'select-checkbox',
              "targets": [ 0 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 7 ]
            }
          ],
          "select": {
            style: 'single'
          },
          "scrollX": true,
          "paging": true,
          "ordering": true,
          "scrollCollapse": true,
          "order": [[ 1, "asc" ]],
          "info":     true,
          "lengthMenu": [[largo], [largo]],
          "dom": 'Bfrtip',
          "language": {
              "zeroRecords": "No tiene personal bajo su cargo",
              "info": "Registro _START_ de _END_ de _TOTAL_",
              "infoEmpty": "No tiene personal bajo su cargo",
              "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente"
              },
              "search": "Buscar: ",
              "select": {
                  "rows": "- %d registros seleccionados"
              },
              "infoFiltered": "(Filtrado de _MAX_ registros)"
          },
          "destroy": true,
          "autoWidth": false,
          "initComplete": function(){
            $('#contenido').fadeIn();
            $('#menu-lateral').fadeIn();
            $('#footer').fadeIn();

            var table = $('#tablaPersonal').DataTable();

            var cuerpoManoPersonal = '';
            cuerpoManoPersonal += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(4).data().unique().length; i++){
              if(table.column(4).data().unique()[i] !== null){
                cuerpoManoPersonal += '<option value="' + table.column(4).data().unique()[i] + '">' + table.column(4).data().unique()[i] + '</option>';
              }
            }
            $("#manoPersonal").html(cuerpoManoPersonal);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#manoPersonal").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoFuncionPersonal = '';
            cuerpoFuncionPersonal += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(3).data().unique().length; i++){
              if(table.column(3).data().unique()[i] !== null){
                cuerpoFuncionPersonal += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
              }
            }
            $("#funcionPersonal").html(cuerpoFuncionPersonal);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#funcionPersonal").select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoComunaPersonal = '';
            cuerpoComunaPersonal += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(6).data().unique().length; i++){
              if(table.column(6).data().unique()[i] !== null){
                arrDatos = table.column(6).data().unique()[i].split(',');
                for(var j = 0; j < arrDatos.length; j++){
                  cuerpoComunaPersonal += '<option value="' + arrDatos[j] + '">' + arrDatos[j] + '</option>';
                }
              }
            }
            $("#comunaSelectPersonal").html(cuerpoComunaPersonal);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#comunaSelectPersonal").select2({
                  theme: "bootstrap"
              });
            }

            setTimeout(function(){
              $('#tablaPersonal').DataTable().columns.adjust();
            },500);
          }
      });
      await esconderMenu();
      $('#tablaPersonal').DataTable().on("draw", function(){
          mensajeWsspSinTexto();
      });
    },1500);
});

app.controller("mantenedorAreaFunController", function(){
    clearInterval(lineaTiempo);
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);

    setTimeout(async function(){
      var largo = Math.trunc(($(window).height() - ($(window).height()/100)*50)/30);
      await $('#tablaAreaFuncional').DataTable( {
          ajax: {
              url: "controller/datosAreaFuncionalTabla.php",
              type: 'POST'
          },
          columns: [
              { data: 'S' },
              { data: 'COMUNA', className: "pointerDatatable" } ,
              { data: 'CODIGOREGION', className: "pointerDatatableCenter" },
              { data: 'OPERA', className: "pointerDatatableCenter" },
              { data: 'OPERA_TEXTO', className: "pointerDatatableCenter" },
              { data: 'IDAREAFUNCIONAL', className: "pointerDatatable" }
          ],
          // responsive: true,
          buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 1,2,3,4]
                },
                title: null,
                text: '<span class="far fa-file-excel"></span>&nbsp;&nbsp;Excel'
            },
            {
              text: '<span class="fas fa-broom"></span>&nbsp;&nbsp;Deseleccionar todo',
              action: function ( e, dt, node, config ) {
                var table = $('#tablaAreaFuncional').DataTable();
                $("#aplicarAreaFuncional").attr("disabled","disabled");
      					$("#quitarAreaFuncional").attr("disabled","disabled");
                table.rows().deselect();
              }
            }
          ],
          "columnDefs": [
            {
              "width": "5px",
              "targets": 0
            },
            {
              "orderable": false,
              "className": 'select-checkbox',
              "targets": [ 0 ]
            },
            {
              "visible": false,
              "searchable": true,
              "targets": [ 4 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 5 ]
            }
          ],
          "select": {
            style: 'multi'
          },
          "scrollX": true,
          "paging": true,
          "ordering": true,
          "scrollCollapse": true,
          // "order": [[ 3, "asc" ]],
          "info":     true,
          "lengthMenu": [[largo], [largo]],
          "dom": 'Bfrtip',
          "language": {
              "zeroRecords": "No tiene personal bajo su cargo",
              "info": "Registro _START_ de _END_ de _TOTAL_",
              "infoEmpty": "No tiene personal bajo su cargo",
              "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente"
              },
              "search": "Buscar: ",
              "select": {
                  "rows": "- %d registros seleccionados"
              },
              "infoFiltered": "(Filtrado de _MAX_ registros)"
          },
          "destroy": true,
          "autoWidth": false,
          "initComplete": function(){
            $('#contenido').fadeIn();
            $('#menu-lateral').fadeIn();
            $('#footer').fadeIn();

            var table = $('#tablaAreaFuncional').DataTable();

            var operaAreaFuncional = '';
            operaAreaFuncional += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(4).data().unique().length; i++){
              if(table.column(4).data().unique()[i] !== null){
                operaAreaFuncional += '<option value="' + table.column(4).data().unique()[i] + '">' + table.column(4).data().unique()[i] + '</option>';
              }
            }
            $("#operaAreaFuncional").html(operaAreaFuncional);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#operaAreaFuncional").select2({
                  theme: "bootstrap"
              });
            }

            var regionAreaFuncional = '';
            regionAreaFuncional += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(2).data().unique().length; i++){
              if(table.column(2).data().unique()[i] !== null){
                regionAreaFuncional += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
              }
            }
            $("#regionAreaFuncional").html(regionAreaFuncional);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#regionAreaFuncional").select2({
                  theme: "bootstrap"
              });
            }

            setTimeout(function(){
              $('#tablaAreaFuncional').DataTable().columns.adjust();
            },500);
          }
      });
      await esconderMenu();
      mensajeWsspSinTexto();
      $('#tablaAreaFuncional').DataTable().on("draw", function(){
          mensajeWsspSinTexto();
      });
      var element = document.getElementById('contenido');
      new ResizeSensor(element, function() {
          $('#tablaAreaFuncional').DataTable().columns.adjust();
      });
    },1500);
});

app.controller("usuariosController", function(){
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);

    setTimeout(async function(){
      var largo = Math.trunc(($(window).height() - ($(window).height()/100)*50)/30);
      // console.log(largo);
      await $('#tablaListadoUsuarios').DataTable( {
          ajax: {
              url: "controller/datosUsuarios.php",
              type: 'POST'
          },
          columns: [
              { data: 'S'},
              { data: 'RUT' } ,
              { data: 'NOMBRE' },
              { data: 'EMAIL'},
              { data: 'PERFIL' },
              { data: 'ESTADO' }
          ],
          /*rowReorder: {
            selector: 'td:nth-child(2)'
          },*/

          buttons: [
              {
                extend: 'excel',
                exportOptions: {
                  columns: [ 1,2,3,4,5 ]
                },
                title: null,
                text: '<span class="far fa-file-excel"></span>&nbsp;&nbsp;Excel'
              }
          ],
          "columnDefs": [
            {
              "width": "5px",
              "targets": 0
            },
            {
              "orderable": false,
              "className": 'select-checkbox',
              "targets": [ 0 ]
            },
            /*{
              "visible": false,
              "searchable": false,
              "targets": [ 29 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 30 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 31 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 32 ]
            },
            {
              "visible": false,
              "searchable": false,
              "targets": [ 33 ]
            }*/
          ],
          "select": {
              style: 'single'
          },
          "scrollX": true,
          "paging": true,
          "ordering": true,
          "scrollCollapse": true,
          // "order": [[ 3, "asc" ]],
          "info":     true,
          "lengthMenu": [[largo], [largo]],
          "dom": 'Bfrtip',
          "language": {
            "zeroRecords": "No hay datos disponibles",
            "info": "Registro _START_ de _END_ de _TOTAL_",
            "infoEmpty": "No hay datos disponibles",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
              },
              "search": "Buscar: ",
              "select": {
                  "rows": "- %d registros seleccionados"
              },
              "infoFiltered": "(Filtrado de _MAX_ registros)"
          },
          "destroy": true,
          "autoWidth": false,
          "initComplete": function( settings, json){
            $('#contenido').fadeIn();
            $('#menu-lateral').fadeIn();
            $('#footer').fadeIn();
            $('#tablaListadoUsuarios').DataTable().columns.adjust();
          }
      });
      await esconderMenu();
      $('#tablaListadoUsuarios').DataTable().on("draw", function(){
      $('#modalAlertasSplash').modal('hide');
      });
    },1500);
});

app.controller("lineaTacController", function(){
    setTimeout(function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },100);

    setTimeout(async function(){
      $('#contenido').fadeIn();
      $('#menu-lateral').fadeIn();
      $('#footer').fadeIn();

      w = $(document).width();
      h = $(document).height() - 170;

      $("#timeline").css("height", h);

      var hoy = moment().format('YYYY-MM-DD');
      $("#fechaTacLina").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        // minDate: min,
        // maxDate: min,
        yearRange: '1920:2040',
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
      });
      $("#fechaTacLina").val(hoy.toString());

      dibujaOTDthTac($("#fechaTacLina").val());

      lineaTacTiempo = setInterval(function (e) {
        dibujaOTDthTac($("#fechaTacLina").val());
      }, 60000);

      var element = document.getElementById('contenido');
      new ResizeSensor(element, function() {
        dibujaOTDthTac($("#fechaTacLina").val());
      });

      $("#mapaTac").css("height",$(window).height()-300);

      await esconderMenu();
      mensajeWsspSinTexto();
    },1500);
});
