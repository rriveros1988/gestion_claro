//Recarga
$(window).on("load",function(event){
  event.preventDefault();
  event.stopImmediatePropagation();
  var tiempo1 = moment(new Date());
  $(document).mousemove(function(event){
    tiempo2 = moment(new Date());
    if(tiempo2.diff(tiempo1, 'seconds') > 20){
      $.ajax({
        url:   'controller/checkToken.php',
        type:  'post',
        success: function (response) {
          if(response !== 'TOKEN_NO'){
            $.ajax({
              url: 'controller/cookie.php',
              type: 'post',
              success: function (response){
              }
            });
          }
        }
      });
      tiempo1 = tiempo2;
    }
	});
  document.addEventListener('touchstart', function(event){
  //Comprobamos si hay varios eventos del mismo tipo
    if (event.targetTouches.length == 1) {
    var touch = event.targetTouches[0];
    // con esto solo se procesa UN evento touch
      tiempo2 = moment(new Date());
      if(tiempo2.diff(tiempo1, 'seconds') > 20){
        $.ajax({
          url:   'controller/checkToken.php',
          type:  'post',
          success: function (response) {
            if(response !== 'TOKEN_NO'){
              $.ajax({
                url: 'controller/cookie.php',
                type: 'post',
                success: function (response){
                }
              });
            }
          }
        });
        tiempo1 = tiempo2;
      }
    }
  });
  $.ajax({
      url:   'controller/checkToken.php',
      type:  'post',
      success: function (response) {
        if(response === 'TOKEN_NO'){
          $(".contenedor-logos").css("display","none");
          $(".contenedor-logos").find('li').css("display","none");
          window.location.href = "#/home";
          $("#logoLinkWeb").fadeOut();
          $("#logoMenu").fadeOut();
          $("#lineaMenu").fadeOut();
          $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
          $("#menu-lateral").css("width","45px");
          $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
          $("#logoMenu").css("color","black");
          $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
          $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
          $("#DivPrincipalMenu").empty();
        }
        else{
          $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success: async function (response) {
                var p = jQuery.parseJSON(response);
                var size = Object.size(p.aaData)/2;
                if(size > 0){
                  if(p.aaData['ESTADO'] === 'Activo'){
                    if($("#DivPrincipalMenu").children().length <= 0){
                      await $.ajax({
                        url:   'controller/datosAreasComunesPadresSolo.php',
                        type:  'post',
                        success: function (response2) {
                          var p2 = jQuery.parseJSON(response2);
                          if(p2.aaData.length !== 0){
                            for(var i = 0; i < p2.aaData.length; i++){
                              var padre = "<div id='" + p2.aaData[i].PADRE + "' style='display:none;' class='contenedor-logos'><div class='logo'><span class='imgMenu " + p2.aaData[i].ICONOPADRE + "'></span></div><p class='title-menu'>" + p2.aaData[i].TEXTOPADRE + "</p></div>";

                              $("#DivPrincipalMenu").append(padre);

                              if(p2.aaData[i].PADRE === "liCuenta"){
                                $("#" + p2.aaData[i].PADRE).append("<div class='sub-menu' style='display:none; color: white; font-size: 12px; margin-left: 15px; margin-bottom: 8px;'><div id='nombrePerfil' class='div-sub-menu'></div></div>");
                              }
                              // $('div[id $=' + p2.aaData[i].PADRE + ']').fadeIn();
                              // $('li[id $=' + p2.aaData[i].NOMBRE + ']').fadeIn();
                              // $('div[id $=' + p2.aaData[i].PADRE + ']').css("display","block");
                              // $('li[id $=' + p2.aaData[i].NOMBRE + ']').css("display","block");
                            }
                            var atras = "<div id='regresarMenu' style='display: none;' class='contenedor-logos'><div class='logo'><span class='imgMenu fas fa-arrow-left'></span></div></div>";

                            $("#DivPrincipalMenu").append(atras);
                          }
                        }
                      });
                      await $.ajax({
                        url:   'controller/datosAreasComunesPadres.php',
                        type:  'post',
                        success: function (response2) {
                          var p2 = jQuery.parseJSON(response2);
                          if(p2.aaData.length !== 0){
                            for(var i = 0; i < p2.aaData.length; i++){
                              var hijo = "<div class='sub-menu' style='display: none; color: white;'><ul><li id='" + p2.aaData[i].NOMBRE + "' style='display: none;'><div class='div-sub-menu'><i class='imgMenu-sub " + p2.aaData[i].ICONO + "'></i><a href='" + p2.aaData[i].RUTA + "' class='title-menu-sub'>" + p2.aaData[i].TEXTO + "</a></div></li></ul></div>";

                              $("#" + p2.aaData[i].PADRE).append(hijo);

                            }
                          }
                        }
                      });

                      n = p.aaData['NOMBRE'].split(" ");
                      if(n.length <= 3){
                        $("#nombrePerfil").html(p.aaData['NOMBRE']);
                      }
                      else{
                        $("#nombrePerfil").html(n[0] + ' ' + n[2] + ' ' + n[3]);
                      }

                      $("#menu-lateral").unbind('click').hover(function(){

                      },
                      function(){
                        $("div.sub-menu").parent().css("height", "45px");
                        $("div.sub-menu").parent().css("background","rgba(30, 0, 0, 0.0)");
                        $("div.sub-menu").fadeOut();
                        $("div.sub-menu").parent().find("p").css("color", "white");
                      });

                      $(".title-menu-sub").unbind('click').hover(function(){
                        $(this).css("color", "yellow");
                      },
                      function(){
                        $(this).css("color", "white");
                      });

                      $(".contenedor-logos").unbind('click').hover(function(){
                        $(this).css({'cursor': 'pointer'});
                      });

                      $(".contenedor-logos").unbind('click').click(function(){
                        var a = $(this).find("p").css("color");

                        //Cierra todo
                        $("div.sub-menu").parent().css("height", "45px");
                        $("div.sub-menu").parent().css("background","rgba(30, 0, 0, 0.0)");
                        $("div.sub-menu").fadeOut();
                        $("div.sub-menu").parent().find("p").css("color", "white");

                        $("#DivPrincipalMenu").children().css("display","none");

                        $(this).css("display","block");

                        $("#regresarMenu").css("display","block");

                        if(a !== "rgb(255, 255, 0)"){
                          //Abre el clickado
                          $(this).find("p").css("color", "yellow");
                          $(this).find("div.sub-menu").fadeIn();
                          var a = $(this).find('li[style*="block"]').length;
                          if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            if($(this).attr("id") == 'liCuenta'){
                              a = (a+1)*28 + 30;
                            }
                            else{
                              a = a*28 + 30;
                            }
                          }
                          else{
                            if($(this).attr("id") == 'liCuenta'){
                              a = (a+1)*25 + 30;
                            }
                            else{
                              a = a*25 + 30;
                            }
                          }
                          $(this).css("height", a + "pt");
                          $(this).css("background-color","#004379");
                        }
                        else{
                          $("#DivPrincipalMenu").children().css("display","block");
                        }
                      });

                      $("#regresarMenu").unbind("click").click(function(){
                        $("div.sub-menu").parent().css("height", "45px");
                        $("div.sub-menu").parent().css("background","rgba(30, 0, 0, 0.0)");
                        $("div.sub-menu").fadeOut();
                        $("div.sub-menu").parent().find("p").css("color", "white");
                        $("#DivPrincipalMenu").children().css("display","block");
                        $("#regresarMenu").css("display","none");
                      });
                    }
                    $.ajax({
                      url:   'controller/datosAreasComunesPadres.php',
                      type:  'post',
                      success: function (response2) {
                        var p2 = jQuery.parseJSON(response2);
                        if(p2.aaData.length !== 0){
                          $(".contenedor-logos").css("display","none");
                          $(".contenedor-logos").find('li').css("display","none");
                          $("#sesionActiva").val("1");
                          $("#sesionActivaUso").val("0");
                          $("#logoMenu").fadeIn();
                          // if(p.aaData['PERFIL'] === 'Tecnico'){
                          //   window.location.href = "#/asignadasTac";
                          // }
                          // else{
                          //   window.location.href = "#/asignadas";
                          // }
                        }
                      }
                    });
                  }
                  else{
                    $(".contenedor-logos").css("display","none");
                    $(".contenedor-logos").find('li').css("display","none");
                    window.location.href = "#/home";
                    $("#logoLinkWeb").fadeOut();
                    $("#logoMenu").fadeOut();
                    $("#lineaMenu").fadeOut();
                    $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                    $("#menu-lateral").css("width","45px");
                    $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                    $("#logoMenu").css("color","black");
                    $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                    $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                    $("#DivPrincipalMenu").empty();
                  }
                }
                else{
                  $(".contenedor-logos").css("display","none");
                  $(".contenedor-logos").find('li').css("display","none");
                  window.location.href = "#/home";
                  $("#logoLinkWeb").fadeOut();
                  $("#logoMenu").fadeOut();
                  $("#lineaMenu").fadeOut();
                  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                  $("#menu-lateral").css("width","45px");
                  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                  $("#logoMenu").css("color","black");
                  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                  $("#DivPrincipalMenu").empty();
                }
              }
          });
        }
      },
      complete: function(){
        $('#contenido').fadeIn();
        $('#footer').fadeIn();
        $('#menu-lateral').fadeIn();
      }
  });
  setInterval(function(){
    if($("#sesionActiva").val() != "0"){
      $.ajax({
        url:   'controller/checkToken.php',
        type:  'post',
        success: function (response) {
          if(response === 'TOKEN_NO'){
            $.ajax({
                url:   'controller/cerraSesion.php',
                type:  'post',
                success: function (response) {
                  $(".modal").modal("hide");
                  $(".contenedor-logos").css("display","none");
                  $(".contenedor-logos").find('li').css("display","none");
                  $("#sesionActiva").val("0");
                  $("#sesionActivaUso").val("0");
                  window.location.href = "#/home";
                  setTimeout(function(){
                    $("#cerradoInactivo").show();
                  },300);
                  $("#logoLinkWeb").fadeOut();
                  $("#logoMenu").fadeOut();
                  $("#lineaMenu").fadeOut();
                  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                  $("#menu-lateral").css("width","45px");
                  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                  $("#logoMenu").css("color","black");
                  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                  $("#DivPrincipalMenu").empty();
                }
            });
          }
        }
      });
    }
  },60000);
});

//Login a sistema
$("#loginSystem-submit").unbind('click').click(function(){
    var URLactual = window.location;
    var parametros = {
        "pass" : $("#loginSystem-pass").val(),
        "rut" : $("#loginSystem-rut").val().replace('.','').replace('.',''),
        "url" : URLactual.toString()
    };
    $.ajax({
        data:  parametros,
        url:   'controller/datosUsuarioConectado.php',
        type:  'post',
        beforeSend: function(){
            $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
            $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
            $('#modalAlertasSplash').modal('show');
        },
        success: function (response) {
          var p = jQuery.parseJSON(response);
          if(p.aaData.length !== 0){
            if(p.aaData[0]['CHECK'] == 'NO'){
              $.ajax({
                  data:  parametros,
                  url:   'controller/datosLogin.php',
                  type:  'post',
                  beforeSend: function(){
                      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
                      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
                      $('#modalAlertasSplash').modal('show');
                  },
                  success: async function (response) {
                    var p = jQuery.parseJSON(response);
                    var size = Object.size(p.aaData)/2;
                    if(size > 0){
                      if(p.aaData['ESTADO'] === 'Activo'){
                        $("#sesionActiva").val("1");
                        $("#sesionActivaUso").val("0");
                        $("#logoMenu").fadeIn();

                        await $.ajax({
                          url:   'controller/datosAreasComunesPadresSolo.php',
                          type:  'post',
                          success: function (response2) {
                            var p2 = jQuery.parseJSON(response2);
                            if(p2.aaData.length !== 0){
                              for(var i = 0; i < p2.aaData.length; i++){
                                var padre = "<div id='" + p2.aaData[i].PADRE + "' style='display:none;' class='contenedor-logos'><div class='logo'><span class='imgMenu " + p2.aaData[i].ICONOPADRE + "'></span></div><p class='title-menu'>" + p2.aaData[i].TEXTOPADRE + "</p></div>";

                                $("#DivPrincipalMenu").append(padre);

                                if(p2.aaData[i].PADRE === "liCuenta"){
                                  $("#" + p2.aaData[i].PADRE).append("<div class='sub-menu' style='display:none; color: white; font-size: 12px; margin-left: 15px; margin-bottom: 8px;'><div id='nombrePerfil' class='div-sub-menu'></div></div>");
                                }
                                // $('div[id $=' + p2.aaData[i].PADRE + ']').fadeIn();
                                // $('li[id $=' + p2.aaData[i].NOMBRE + ']').fadeIn();
                                // $('div[id $=' + p2.aaData[i].PADRE + ']').css("display","block");
                                // $('li[id $=' + p2.aaData[i].NOMBRE + ']').css("display","block");
                              }
                              var atras = "<div id='regresarMenu' style='display: none;' class='contenedor-logos'><div class='logo'><span class='imgMenu fas fa-arrow-left'></span></div></div>";

                              $("#DivPrincipalMenu").append(atras);
                            }
                          }
                        });
                        await $.ajax({
                          url:   'controller/datosAreasComunesPadres.php',
                          type:  'post',
                          success: function (response2) {
                            var p2 = jQuery.parseJSON(response2);
                            if(p2.aaData.length !== 0){
                              for(var i = 0; i < p2.aaData.length; i++){
                                var hijo = "<div class='sub-menu' style='display: none; color: white;'><ul><li id='" + p2.aaData[i].NOMBRE + "' style='display: none;'><div class='div-sub-menu'><i class='imgMenu-sub " + p2.aaData[i].ICONO + "'></i><a href='" + p2.aaData[i].RUTA + "' class='title-menu-sub'>" + p2.aaData[i].TEXTO + "</a></div></li></ul></div>";

                                $("#" + p2.aaData[i].PADRE).append(hijo);

                              }
                            }
                          }
                        });

                        n = p.aaData['NOMBRE'].split(" ");
                        if(n.length <= 3){
                          $("#nombrePerfil").html(p.aaData['NOMBRE']);
                        }
                        else{
                          $("#nombrePerfil").html(n[0] + ' ' + n[2] + ' ' + n[3]);
                        }

                        $("#menu-lateral").unbind('click').hover(function(){

                        },
                        function(){
                          $("div.sub-menu").parent().css("height", "45px");
                          $("div.sub-menu").parent().css("background","rgba(30, 0, 0, 0.0)");
                          $("div.sub-menu").fadeOut();
                          $("div.sub-menu").parent().find("p").css("color", "white");
                        });

                        $(".title-menu-sub").unbind('click').hover(function(){
                          $(this).css("color", "yellow");
                        },
                        function(){
                          $(this).css("color", "white");
                        });

                        $(".contenedor-logos").unbind('click').hover(function(){
                          $(this).css({'cursor': 'pointer'});
                        });

                        $(".contenedor-logos").unbind('click').click(function(){
                          var a = $(this).find("p").css("color");

                          //Cierra todo
                          $("div.sub-menu").parent().css("height", "45px");
                          $("div.sub-menu").parent().css("background","rgba(30, 0, 0, 0.0)");
                          $("div.sub-menu").fadeOut();
                          $("div.sub-menu").parent().find("p").css("color", "white");

                          $("#DivPrincipalMenu").children().css("display","none");

                          $(this).css("display","block");

                          $("#regresarMenu").css("display","block");

                          if(a !== "rgb(255, 255, 0)"){
                            //Abre el clickado
                            $(this).find("p").css("color", "yellow");
                            $(this).find("div.sub-menu").fadeIn();
                            var a = $(this).find('li[style*="block"]').length;
                            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                              if($(this).attr("id") == 'liCuenta'){
                                a = (a+1)*28 + 30;
                              }
                              else{
                                a = a*28 + 30;
                              }
                            }
                            else{
                              if($(this).attr("id") == 'liCuenta'){
                                a = (a+1)*25 + 30;
                              }
                              else{
                                a = a*25 + 30;
                              }
                            }
                            $(this).css("height", a + "pt");
                            $(this).css("background-color","#004379");
                          }
                          else{
                            $("#DivPrincipalMenu").children().css("display","block");
                          }
                        });

                        $("#regresarMenu").unbind("click").click(function(){
                          $("div.sub-menu").parent().css("height", "45px");
                          $("div.sub-menu").parent().css("background","rgba(30, 0, 0, 0.0)");
                          $("div.sub-menu").fadeOut();
                          $("div.sub-menu").parent().find("p").css("color", "white");
                          $("#DivPrincipalMenu").children().css("display","block");
                          $("#regresarMenu").css("display","none");
                        });

                        var random = Math.round(Math.random() * (1000000 - 1) + 1);
                        if(p.aaData['PERFIL'] === 'Tecnico'){
                          window.location.href = "?idLog=" + random + "#/asignadasTac";
                        }
                        else{
                          window.location.href = "?idLog=" + random + "#/asignadas";
                        }
                      }
                      else{
                        $("#buttonAceptarAlerta").css("display","inline");
                        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                        $("#textoModal").html("El usuario ingresado no se encuentra activo en sistema");
                        setTimeout(function(){
                          $('#modalAlertasSplash').modal('hide');
                        },500);
                        $('#modalAlertas').modal('show');
                      }
                    }
                    else{
                      $("#buttonAceptarAlerta").css("display","inline");
                      $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                      $("#textoModal").html("Datos de acceso incorrectos o no registrados en sistema");
                      setTimeout(function(){
                        $('#modalAlertasSplash').modal('hide');
                      },500);
                      $('#modalAlertas').modal('show');
                    }
                  }
              });
            }
            else{
              $("#buttonAceptarAlerta").css("display","inline");
              $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
              $("#textoModal").html("El usuario ingresado ya se encuentra conectado al sistema");
              setTimeout(function(){
                $('#modalAlertasSplash').modal('hide');
              },500);
              $('#modalAlertas').modal('show');
            }
          }
          else{
            $("#buttonAceptarAlerta").css("display","inline");
            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#textoModal").html("Datos de acceso incorrectos o no registrados en sistema");
            setTimeout(function(){
              $('#modalAlertasSplash').modal('hide');
            },500);
            $('#modalAlertas').modal('show');
          }
        }
    });
});

//Verificar input quita borde rojo
$("#loginSystem-rut").on('input', function(){
  $(this).css("border","");
});

//Verifica rut
function Rut()
{
  var texto = window.document.getElementById("loginSystem-rut").value;
  var tmpstr = "";
  for ( i=0; i < texto.length ; i++ )
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
      tmpstr = tmpstr + texto.charAt(i);
  texto = tmpstr;
  largo = texto.length;

  if(texto == ""){
    return false;
  }
  else if ( largo < 2 )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }

  for (i=0; i < largo ; i++ )
  {
    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
    {
      $("#buttonAceptarAlerta").css("display","inline");
      $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
      $('#modalAlertas').modal('show');
      window.document.getElementById("loginSystem-rut").value = "";
      $("#loginSystem-rut").css("border","1px solid red");
      window.document.getElementById("loginSystem-rut").focus();
      window.document.getElementById("loginSystem-rut").select();
      return false;
    }
  }

  var invertido = "";
  for ( i=(largo-1),j=0; i>=0; i--,j++ )
    invertido = invertido + texto.charAt(i);
  var dtexto = "";
  dtexto = dtexto + invertido.charAt(0);
  dtexto = dtexto + '-';
  cnt = 0;

  for ( i=1,j=2; i<largo; i++,j++ )
  {
    //alert("i=[" + i + "] j=[" + j +"]" );
    if ( cnt == 3 )
    {
      dtexto = dtexto + '.';
      j++;
      dtexto = dtexto + invertido.charAt(i);
      cnt = 1;
    }
    else
    {
      dtexto = dtexto + invertido.charAt(i);
      cnt++;
    }
  }

  invertido = "";
  for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
    invertido = invertido + dtexto.charAt(i);

  window.document.getElementById("loginSystem-rut").value = invertido.toUpperCase()

  if ( revisarDigito(texto) )
    return true;

  return false;
}

function revisarDigito2( dvr )
{
  dv = dvr + ""
  if ( dv != "" && dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar un digito verificador válido");
    $('#modalAlertas').modal('show');
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  return true;
}

function revisarDigito( crut )
{
  largo = crut.length;
  if(crut == ""){
    return false;
  }
  else if ( largo < 2)
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  if ( largo > 2 )
    rut = crut.substring(0, largo - 1);
  else
    rut = crut.charAt(0);
  dv = crut.charAt(largo-1);
  revisarDigito2( dv );

  if ( rut == null || dv == null )
    return 0

  var dvr = '0'
  suma = 0
  mul  = 2

  for (i= rut.length -1 ; i >= 0; i--)
  {
    suma = suma + rut.charAt(i) * mul
    if (mul == 7)
      mul = 2
    else
      mul++
  }
  res = suma % 11
  if (res==1)
    dvr = 'k'
  else if (res==0)
    dvr = '0'
  else
  {
    dvi = 11-res
    dvr = dvi + ""
  }
  if ( dvr != dv.toLowerCase() )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false
  }

  return true
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// Interaccion con envio de wssp
$("#btnWssp").unbind("click").click(function(){
  if(parseFloat($('#btnWssp').css("opacity")) > parseFloat(0.2)){
    $("#mensajeWssp").fadeOut();
    $('#btnWssp').css("opacity",0.2);
  }
  else{
    $("#bocadilloWssp").hide();
    $("#mensajeWssp").fadeIn();
    $('#btnWssp').css("opacity",1);
  }
});

$("#cierraTextoWssp").unbind("click").click(function(){
  $("#mensajeWssp").fadeOut();
  $('#btnWssp').css("opacity",0.2);
});

$("#btnEnviarTextoWssp").unbind("click").click(function(){
  if($("#textoWssp").val() !== ''){
      window.open("https://api.whatsapp.com/send?phone=56968315567&text=" + $("#textoWssp").val(),'_blank');
      $("#textoWssp").val('');
      $("#mensajeWssp").fadeOut();
  }
  else{
    $("#buttonAceptarAlerta").css("display","inline");
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("Favor ingrese un mensaje a enviar");
    $('#modalAlertas').modal('show');
  }
});

$('.input-number').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
});

$('.input-money').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
});

$('.input-money').unbind("click").change(function(){
  this.value = accounting.formatMoney(this.value, "$ ", 0);
});

$("#reagendamientoOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#reagendamientoOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('12').search(filtro).draw();
});

$("#regionOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#regionOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('13').search(filtro).draw();
});

$("#comunaOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#comunaOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('14').search(filtro).draw();
});

$("#origenOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#origenOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('2').search(filtro).draw();
});

$("#tticketOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#tticketOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('3').search(filtro).draw();
});

$("#fechaCompOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#fechaCompOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('7').search(filtro).draw();
});

$("#rangoOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#rangoOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('9').search(filtro).draw();
});

$("#despachoOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#despachoOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('20').search(filtro).draw();
});

$("#estadoOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#estadoOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('5').search(filtro).draw();
});

$("#semafotoOtDTH").unbind("click").change(function(){
  var table = $("#tablaOtDTH").DataTable();
  var filtro = $("#semafotoOtDTH").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('29').search(filtro).draw();
});

async function generaMapa(direccion, folio, run, cliente){
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
	u = 'https://maps.googleapis.com/maps/api/geocode/json?';
	datos = {
		address	: direccion + ', Chile',
		key		: 'AIzaSyCb15rFHKSkFxmZbQIo6KVes2-GR3N-LcQ'
	};

  var lat = '';
  var lng = '';
	await $.ajax({
		url: u,
		type: 'get',
		dataType: "json",
		data: datos,
		success: function(data){
      lat = data.results[0].geometry.location.lat;
			lng = data.results[0].geometry.location.lng;
		}
	});
  google.charts.load("current", {
    "packages":["map"],
    // Note: you will need to get a mapsApiKey for your project.
    // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
    "mapsApiKey": "AIzaSyCb15rFHKSkFxmZbQIo6KVes2-GR3N-LcQ"
  });

  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Lat', 'Long', 'Name'],
      [lat, lng, 'Dir: ' + direccion],
    ]);

    var options = {
      zoomLevel: 17,
      showTooltip: true,
      showInfoWindow: true,
      mapType: 'terrain',
      icons: {
        default: {
          normal: 'view/img/icon_map/Map-Marker-Ball-Pink-icon.png',
          selected: 'view/img/icon_map/Map-Marker-Ball-Right-Pink-icon.png'
        }
      }
    };

    $("#tituloMapaOT").html('OT: ' + folio + ', RUN: ' + run + ', Nombre Cliente: ' + cliente);
    $('#modalAlertasSplash').modal('hide');
    $("#modalMapaOT").modal({backdrop: 'static', keyboard: false});
    $("#modalMapaOT").modal('show');

    setTimeout(function(){
      var map = new google.visualization.Map(document.getElementById('mapaOTGoogle'));
      map.draw(data, options);
    },200);
  }
}

$("#cerrarModalMapaOT").unbind("click").click(function(){
  $("#mapaOTGoogle").html('');
  $('#modalAlertasSplash').modal('hide');
  $("#modalMapaOT").modal('hide');
});

$("#manoPersonal").unbind("click").change(function(){
  var table = $("#tablaPersonal").DataTable();
  var mano = $("#manoPersonal").val();
  if(mano == "Todos"){
    mano = '';
  }
  table.rows('').deselect();
  table.columns('4').search(mano).draw();
});

$("#funcionPersonal").unbind("click").change(function(){
  var table = $("#tablaPersonal").DataTable();
  var mano = $("#funcionPersonal").val();
  if(mano == "Todos"){
    mano = '';
  }
  table.rows('').deselect();
  table.columns('3').search(mano).draw();
});

$("#comunaSelectPersonal").unbind("click").change(function(){
  var table = $("#tablaPersonal").DataTable();
  var mano = $("#comunaSelectPersonal").val();
  if(mano == "Todos"){
    mano = '';
  }
  table.rows('').deselect();
  table.columns('6').search(mano).draw();
});

$("#cerrarAsignarComuna").unbind("click").click(async function(){
  console.log("Ok");
  $("#comunaAsignarComuna").multiSelect('deselect_all');
  $("#selectComuna").val('');
  $("#dispoComuna").val('');
});

$("#comunaPersonal").unbind("click").click(async function(){
  var table = $('#tablaPersonal').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.DNI;
  });
  var nombre = $.map(table.rows('.selected').data(), function (item) {
      return item.NOMBRE;
  });
  var comuna = $.map(table.rows('.selected').data(), function (item) {
      return item.IDCOMUNA;
  });
  $("#tituloAsignarComuna").html("Rut: " + rutUsuario[0] + ' , Nombre: ' +nombre[0]);

  await $.ajax({
    url:   'controller/datosAreaFuncional.php',
    type:  'post',
    success: function (response2) {
      var cuerpoAF = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoAF += "<option value=" + p.aaData[i].IDAREAFUNCIONAL + ">&nbsp;&nbsp;" + p.aaData[i].COMUNA + ', Región: ' + p.aaData[i].CODIGOREGION + "</option>";
        }
        $("#comunaAsignarComuna").html(cuerpoAF);

        if(comuna.length > 0){
          arregloComunas = comuna[0].split(',');
          $.each(arregloComunas, function(i,e){
              $("#comunaAsignarComuna option[value='" + e + "']").prop("selected", true);
          });
          console.log(arregloComunas);
        }
      }
    }
  });

  $("#comunaAsignarComuna").multiSelect({
    selectableFooter: "<div class='custom-header'>&nbsp;Disponibles</div>",
    selectionFooter: "<div class='custom-header'>&nbsp;Seleccionadas</div>",
    selectableHeader: "<input id='dispoComuna' type='text' class='search-input form-control' autocomplete='off' placeholder='Buscar'>",
    selectionHeader: "<input id='selectComuna' type='text' class='search-input form-control' autocomplete='off' placeholder='Buscar'>",
    afterInit: function(ms){
      var that = this,
          $selectableSearch = that.$selectableUl.prev(),
          $selectionSearch = that.$selectionUl.prev(),
          selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
          selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40){
          that.$selectableUl.focus();
          return false;
        }
      });

      that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
      .on('keydown', function(e){
        if (e.which == 40){
          that.$selectionUl.focus();
          return false;
        }
      });
    },
    afterSelect: function(){
      this.qs1.cache();
      this.qs2.cache();
    },
    afterDeselect: function(){
      this.qs1.cache();
      this.qs2.cache();
    }
  });

  $("#modalAsignarComuna").modal({backdrop: 'static', keyboard: false});
  $("#modalAsignarComuna").modal("show");
});

$("#guardarAsignarComuna").unbind("claro").click(function(){
  $("#modalAsignarComuna").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaPersonal').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.DNI;
  });
  var parametros = {
    "rut": rutUsuario[0],
    "idareafuncional": $("#comunaAsignarComuna").val()
  }
  $.ajax({
    data:  parametros,
    url:   'controller/actualizarComunaPersonal.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(p != null){
        if(p[0] != "Sin datos" && p[0] != "" && p[0] != 'Error'){
          $("#buttonAceptarAlerta").css("display","inline");
          $('#modalAlertasSplash').modal('hide');
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Comuna asignada correctamente");
          table.ajax.reload();

          setTimeout(function(){
            var cuerpoManoPersonal = '';
            cuerpoManoPersonal += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(4).data().unique().length; i++){
              if(table.column(4).data().unique()[i] !== null){
                cuerpoManoPersonal += '<option value="' + table.column(4).data().unique()[i] + '">' + table.column(4).data().unique()[i] + '</option>';
              }
            }
            $("#manoPersonal").html(cuerpoManoPersonal);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#manoPersonal").select2('destroy').select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoFuncionPersonal = '';
            cuerpoFuncionPersonal += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(3).data().unique().length; i++){
              if(table.column(3).data().unique()[i] !== null){
                cuerpoFuncionPersonal += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
              }
            }
            $("#funcionPersonal").html(cuerpoFuncionPersonal);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#funcionPersonal").select2('destroy').select2({
                  theme: "bootstrap"
              });
            }

            var cuerpoComunaPersonal = '';
            cuerpoComunaPersonal += '<option selected value="Todos">Todos</option>';
            for(var i = 0; i < table.column(6).data().unique().length; i++){
              if(table.column(6).data().unique()[i] !== null){
                arrDatos = table.column(6).data().unique()[i].split(',');
                for(var j = 0; j < arrDatos.length; j++){
                  cuerpoComunaPersonal += '<option value="' + arrDatos[j] + '">' + arrDatos[j] + '</option>';
                }
              }
            }
            $("#comunaSelectPersonal").html(cuerpoComunaPersonal);
            if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#comunaSelectPersonal").select2('destroy').select2({
                  theme: "bootstrap"
              });
            }
            $("#comunaAsignarComuna").multiSelect('deselect_all');
            $("#selectComuna").val('');
            $("#dispoComuna").val('');
          },1000);
        }
        else{
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al asignar la comuna, si el problema persiste favor comuniquese con soporte");
          $('#modalAlertas').modal('show');
          $("#comunaAsignarComuna").multiSelect('deselect_all');
          $("#selectComuna").val('');
          $("#dispoComuna").val('');
        }
      }
    }
  });
});

$("#aplicarAreaFuncional").unbind("click").click(function(){
  var table = $('#tablaAreaFuncional').DataTable();
  var datos = table.rows('.selected').data();
  parametros = [];
  for(var i = 0; i < datos.length; i++){
    parametros.push(datos[i].IDAREAFUNCIONAL);
  }

  $.ajax({
    url:   'controller/activaAreaFuncional.php',
    type:  'post',
    data:  {array : parametros},
    beforeSend: function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },
    success:  function (response) {
      setTimeout(function(){
        table.ajax.reload();
        $("#buttonAceptarAlerta").css("display","inline");
        $('#modalAlertasSplash').modal('hide');
        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Comuna asignada correctamente");
        $('#modalAlertas').modal('show');
      },500);
    }
  });
});

$("#quitarAreaFuncional").unbind("click").click(function(){
  var table = $('#tablaAreaFuncional').DataTable();
  var datos = table.rows('.selected').data();
  parametros = [];
  for(var i = 0; i < datos.length; i++){
    parametros.push(datos[i].IDAREAFUNCIONAL);
  }

  $.ajax({
    url:   'controller/quitaAreaFuncional.php',
    type:  'post',
    data:  {array : parametros},
    beforeSend: function(){
      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
      $('#modalAlertasSplash').modal('show');
    },
    success:  function (response) {
      setTimeout(function(){
        table.ajax.reload();
        $("#buttonAceptarAlerta").css("display","inline");
        $('#modalAlertasSplash').modal('hide');
        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Comuna asignada correctamente");
        $('#modalAlertas').modal('show');
      },500);
    }
  });
});

$("#operaAreaFuncional").unbind("click").change(function(){
  var table = $("#tablaAreaFuncional").DataTable();
  var filtro = $("#operaAreaFuncional").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('4').search(filtro).draw();
});

$("#regionAreaFuncional").unbind("click").change(function(){
  var table = $("#tablaAreaFuncional").DataTable();
  var filtro = $("#regionAreaFuncional").val();
  if(filtro == "Todos"){
    filtro = '';
  }
  table.rows('').deselect();
  table.columns('2').search(filtro).draw();
});

$("#buttonAceptarAlerta").unbind("click").click(function(){
    $('#modalAlertasSplash').modal('hide');
});

$("#asignarDespachoOTDTH").unbind("click").click(async function(){
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  $("#tituloAsignarDespachoOTDTH").html("Cantidad de OT seleccionadas: " + datos.length);
  await $.ajax({
    url:   'controller/datosDespachos.php',
    type:  'post',
    success: function (response2) {
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        var cuerpoDespachosDTH = '';
        for(var i = 0; i < p.aaData.length; i++){
          if(p.aaData[i].COMUNA === null){
            cuerpoDespachosDTH += '<option value="' + p.aaData[i].IDPERSONAL + '">' + p.aaData[i].DNI + ' - ' + p.aaData[i].NOMBRES + '  ' + p.aaData[i].APELLIDOS + ' (Comuna: )' + '</option>';
          }
          else{
            cuerpoDespachosDTH += '<option value="' + p.aaData[i].IDPERSONAL + '">' + p.aaData[i].DNI + ' - ' + p.aaData[i].NOMBRES + '  ' + p.aaData[i].APELLIDOS + ' (Comuna: ' + p.aaData[i].COMUNA +')' + '</option>';
          }
        }
        $("#despachoAsignarDespachoOTDTH").html(cuerpoDespachosDTH);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#despachoAsignarDespachoOTDTH").select2({
        theme: "bootstrap"
    });
  }
  $("#modalAsignarDespachoOTDTH").modal("show");
});

$("#guardarAsignarDespachoOTDTH").unbind("click").click(function(){
  $("#modalAsignarDespachoOTDTH").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  arrayOtDth = [];
  for(var i = 0; i < datos.length; i++){
    arrayOtDth.push(datos[i].FOLIO.split("&nbsp;&nbsp;")[1]);
  }

  var parametros = {
    "idDespacho": $("#despachoAsignarDespachoOTDTH").val(),
    "arrayOTDth": arrayOtDth
  }

  $.ajax({
    url:   'controller/actualizaDespachoOTDth.php',
    type:  'post',
    data:  parametros,
    success: async function (response) {
      var p = jQuery.parseJSON(response);
      if(p.aaData.length !== 0){
        var table = $('#tablaOtDTH').DataTable();
        var rows = table.rows( '.selected' ).remove().draw();
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Despacho asignado correctamente");
        $('#modalAlertas').modal('show');
        $("#agendarOTDTH").attr("disabled","disabled");
        $("#asignarDespachoOTDTH").attr("disabled","disabled");
        $("#asignarTecnicoOTDTH").attr("disabled","disabled");
        $("#estadoOTDTH").attr("disabled","disabled");
        $("#observacionOTDTH").attr("disabled","disabled");

        function ingresaModificacionesOTDth(p){
          var table2 = $('#tablaOtDTH').DataTable();
          for(var k = 0; k < p.aaData.length; k++){
            table2.rows.add([{
              'S': p.aaData[k].S,
              'FOLIO': p.aaData[k].FOLIO,
              'ORIGEN': p.aaData[k].ORIGEN,
              'TIPO_TICKET': p.aaData[k].TIPO_TICKET,
              'ESTADO_MANDANTE': p.aaData[k].ESTADO_MANDANTE,
              'ESTADO_DESPACHO': p.aaData[k].ESTADO_DESPACHO,
              'FECHA_CREACION': p.aaData[k].FECHA_CREACION,
              'FECHA_AGENDA': p.aaData[k].FECHA_AGENDA,
              'FECHA_GESTOR': p.aaData[k].FECHA_GESTOR,
              'RANGO': p.aaData[k].RANGO,
              'DIAS': p.aaData[k].DIAS,
              'DIAS_AGENDA': p.aaData[k].DIAS_AGENDA,
              'CANTIDAD_REAGENDAMIENTOS': p.aaData[k].CANTIDAD_REAGENDAMIENTOS,
              'COSTO': p.aaData[k].COSTO,
              'REGION': p.aaData[k].REGION,
              'COMUNA': p.aaData[k].COMUNA,
              'DIRECCION': p.aaData[k].DIRECCION,
              'RUT': p.aaData[k].RUT,
              'CLIENTE': p.aaData[k].CLIENTE,
              'FONO1': p.aaData[k].FONO1,
              'FONO2': p.aaData[k].FONO2,
              'DESPACHO': p.aaData[k].DESPACHO,
              'TECNICO': p.aaData[k].TECNICO,
              'OBSERVACION_DESPACHO': p.aaData[k].OBSERVACION_DESPACHO,
              'REFERENCIA': p.aaData[k].REFERENCIA,
              'REFERENCIA2': p.aaData[k].REFERENCIA2,
              'OBSERVACION_DESPACHO2': p.aaData[k].OBSERVACION_DESPACHO2,
              'HORA_AGENDA': p.aaData[k].HORA_AGENDA,
              'CALLE': p.aaData[k].CALLE,
              'NUMERO': p.aaData[k].NUMERO,
              'SEMAFORO': p.aaData[k].SEMAFORO
            }]).draw();
          }
        }

        await ingresaModificacionesOTDth(p);

        var filtro = '';

        table.columns('13').search(filtro).draw();
        table.columns('14').search(filtro).draw();
        table.columns('2').search(filtro).draw();
        table.columns('3').search(filtro).draw();
        table.columns('7').search(filtro).draw();
        table.columns('9').search(filtro).draw();
        table.columns('20').search(filtro).draw();
        table.columns('5').search(filtro).draw();
        table.columns('29').search(filtro).draw();


         var cuerpoRegionDTH = '';
         cuerpoRegionDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(13).data().unique().length; i++){
           if(table.column(13).data().unique()[i] !== null){
             cuerpoRegionDTH += '<option value="' + table.column(13).data().unique()[i] + '">' + table.column(13).data().unique()[i] + '</option>';
           }
         }
         $("#regionOtDTH").html(cuerpoRegionDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#regionOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoComunaDTH = '';
         cuerpoComunaDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(14).data().unique().length; i++){
           if(table.column(14).data().unique()[i] !== null){
             cuerpoComunaDTH += '<option value="' + table.column(14).data().unique()[i] + '">' + table.column(14).data().unique()[i] + '</option>';
           }
         }
         $("#comunaOtDTH").html(cuerpoComunaDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#comunaOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOrigenDTH = '';
         cuerpoOrigenDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(2).data().unique().length; i++){
           if(table.column(2).data().unique()[i] !== null){
             cuerpoOrigenDTH += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
           }
         }
         $("#origenOtDTH").html(cuerpoOrigenDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#origenOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoTTicketDTH = '';
         cuerpoTTicketDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(3).data().unique().length; i++){
           if(table.column(3).data().unique()[i] !== null){
             cuerpoTTicketDTH += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
           }
         }
         $("#tticketOtDTH").html(cuerpoTTicketDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#tticketOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpofechaCompDTH = '';
         cuerpofechaCompDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(7).data().unique().length; i++){
           if(table.column(7).data().unique()[i] !== null){
             cuerpofechaCompDTH += '<option value="' + table.column(7).data().unique()[i] + '">' + table.column(7).data().unique()[i] + '</option>';
           }
         }
         $("#fechaCompOtDTH").html(cuerpofechaCompDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#fechaCompOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoRangoDTH = '';
         cuerpoRangoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(9).data().unique().length; i++){
           if(table.column(9).data().unique()[i] !== null){
             cuerpoRangoDTH += '<option value="' + table.column(9).data().unique()[i] + '">' + table.column(9).data().unique()[i] + '</option>';
           }
         }
         $("#rangoOtDTH").html(cuerpoRangoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#rangoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOtDTH = '';
         cuerpoOtDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(20).data().unique().length; i++){
           if(table.column(20).data().unique()[i] !== null){
             cuerpoOtDTH += '<option value="' + table.column(20).data().unique()[i] + '">' + table.column(20).data().unique()[i] + '</option>';
           }
         }
         $("#despachoOtDTH").html(cuerpoOtDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#despachoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoEstadoDTH = '';
         cuerpoEstadoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(5).data().unique().length; i++){
           if(table.column(5).data().unique()[i] !== null){
             cuerpoEstadoDTH += '<option value="' + table.column(5).data().unique()[i] + '">' + table.column(5).data().unique()[i] + '</option>';
           }
         }
         $("#estadoOtDTH").html(cuerpoEstadoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#estadoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoObsDTH = '';
         cuerpoObsDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(29).data().unique().length; i++){
           if(table.column(29).data().unique()[i] !== null){
             cuerpoObsDTH += '<option value="' + table.column(29).data().unique()[i] + '">' + table.column(29).data().unique()[i] + '</option>';
           }
         }
         $("#semafotoOtDTH").html(cuerpoObsDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#semafotoOtDTH").select2({
               theme: "bootstrap"
           });
         }
      }
    }
  });
});

$("#asignarTecnicoOTDTH").unbind("click").click(async function(){
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  $("#tituloAsignarTecnicoOTDTH").html("Cantidad de OT seleccionadas: " + datos.length);
  await $.ajax({
    url:   'controller/datosTecnicos.php',
    type:  'post',
    success: function (response2) {
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        var cuerpoDespachosDTH = '';
        for(var i = 0; i < p.aaData.length; i++){
          if(p.aaData[i].COMUNA === null){
            cuerpoDespachosDTH += '<option value="' + p.aaData[i].IDPERSONAL + '">' + p.aaData[i].DNI + ' - ' + p.aaData[i].NOMBRES + '  ' + p.aaData[i].APELLIDOS + ' (Comuna: )' + '</option>';
          }
          else{
            cuerpoDespachosDTH += '<option value="' + p.aaData[i].IDPERSONAL + '">' + p.aaData[i].DNI + ' - ' + p.aaData[i].NOMBRES + '  ' + p.aaData[i].APELLIDOS + ' (Comuna: ' + p.aaData[i].COMUNA +')' + '</option>';
          }
        }
        $("#despachoAsignarTecnicoOTDTH").html(cuerpoDespachosDTH);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#despachoAsignarTecnicoOTDTH").select2({
        theme: "bootstrap"
    });
  }
  $("#modalAsignarTecnicoOTDTH").modal("show");
});

$("#guardarAsignarTecnicoOTDTH").unbind("click").click(function(){
  $("#modalAsignarTecnicoOTDTH").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  arrayOtDth = [];
  for(var i = 0; i < datos.length; i++){
    arrayOtDth.push(datos[i].FOLIO.split("&nbsp;&nbsp;")[1]);
  }

  var parametros = {
    "$idTecnico": $("#despachoAsignarTecnicoOTDTH").val(),
    "arrayOTDth": arrayOtDth
  }

  $.ajax({
    url:   'controller/actualizaTecnicoOTDth.php',
    type:  'post',
    data:  parametros,
    success: async function (response) {
      var p = jQuery.parseJSON(response);
      if(p.aaData.length !== 0){
        var table = $('#tablaOtDTH').DataTable();
        var rows = table.rows( '.selected' ).remove().draw();
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Técnico asignado correctamente");
        $('#modalAlertas').modal('show');
        $("#agendarOTDTH").attr("disabled","disabled");
        $("#asignarDespachoOTDTH").attr("disabled","disabled");
        $("#asignarTecnicoOTDTH").attr("disabled","disabled");
        $("#estadoOTDTH").attr("disabled","disabled");
        $("#observacionOTDTH").attr("disabled","disabled");

        function ingresaModificacionesOTDth(p){
          var table2 = $('#tablaOtDTH').DataTable();
          for(var k = 0; k < p.aaData.length; k++){
            table2.rows.add([{
              'S': p.aaData[k].S,
              'FOLIO': p.aaData[k].FOLIO,
              'ORIGEN': p.aaData[k].ORIGEN,
              'TIPO_TICKET': p.aaData[k].TIPO_TICKET,
              'ESTADO_MANDANTE': p.aaData[k].ESTADO_MANDANTE,
              'ESTADO_DESPACHO': p.aaData[k].ESTADO_DESPACHO,
              'FECHA_CREACION': p.aaData[k].FECHA_CREACION,
              'FECHA_AGENDA': p.aaData[k].FECHA_AGENDA,
              'FECHA_GESTOR': p.aaData[k].FECHA_GESTOR,
              'RANGO': p.aaData[k].RANGO,
              'DIAS': p.aaData[k].DIAS,
              'DIAS_AGENDA': p.aaData[k].DIAS_AGENDA,
              'CANTIDAD_REAGENDAMIENTOS': p.aaData[k].CANTIDAD_REAGENDAMIENTOS,
              'COSTO': p.aaData[k].COSTO,
              'REGION': p.aaData[k].REGION,
              'COMUNA': p.aaData[k].COMUNA,
              'DIRECCION': p.aaData[k].DIRECCION,
              'RUT': p.aaData[k].RUT,
              'CLIENTE': p.aaData[k].CLIENTE,
              'FONO1': p.aaData[k].FONO1,
              'FONO2': p.aaData[k].FONO2,
              'DESPACHO': p.aaData[k].DESPACHO,
              'TECNICO': p.aaData[k].TECNICO,
              'OBSERVACION_DESPACHO': p.aaData[k].OBSERVACION_DESPACHO,
              'REFERENCIA': p.aaData[k].REFERENCIA,
              'REFERENCIA2': p.aaData[k].REFERENCIA2,
              'OBSERVACION_DESPACHO2': p.aaData[k].OBSERVACION_DESPACHO2,
              'HORA_AGENDA': p.aaData[k].HORA_AGENDA,
              'CALLE': p.aaData[k].CALLE,
              'NUMERO': p.aaData[k].NUMERO,
              'SEMAFORO': p.aaData[k].SEMAFORO
            }]).draw();
          }
        }

        await ingresaModificacionesOTDth(p);

        var filtro = '';

        table.columns('13').search(filtro).draw();
        table.columns('14').search(filtro).draw();
        table.columns('2').search(filtro).draw();
        table.columns('3').search(filtro).draw();
        table.columns('7').search(filtro).draw();
        table.columns('9').search(filtro).draw();
        table.columns('20').search(filtro).draw();
        table.columns('5').search(filtro).draw();
        table.columns('29').search(filtro).draw();


         var cuerpoRegionDTH = '';
         cuerpoRegionDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(13).data().unique().length; i++){
           if(table.column(13).data().unique()[i] !== null){
             cuerpoRegionDTH += '<option value="' + table.column(13).data().unique()[i] + '">' + table.column(13).data().unique()[i] + '</option>';
           }
         }
         $("#regionOtDTH").html(cuerpoRegionDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#regionOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoComunaDTH = '';
         cuerpoComunaDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(14).data().unique().length; i++){
           if(table.column(14).data().unique()[i] !== null){
             cuerpoComunaDTH += '<option value="' + table.column(14).data().unique()[i] + '">' + table.column(14).data().unique()[i] + '</option>';
           }
         }
         $("#comunaOtDTH").html(cuerpoComunaDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#comunaOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOrigenDTH = '';
         cuerpoOrigenDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(2).data().unique().length; i++){
           if(table.column(2).data().unique()[i] !== null){
             cuerpoOrigenDTH += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
           }
         }
         $("#origenOtDTH").html(cuerpoOrigenDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#origenOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoTTicketDTH = '';
         cuerpoTTicketDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(3).data().unique().length; i++){
           if(table.column(3).data().unique()[i] !== null){
             cuerpoTTicketDTH += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
           }
         }
         $("#tticketOtDTH").html(cuerpoTTicketDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#tticketOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpofechaCompDTH = '';
         cuerpofechaCompDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(7).data().unique().length; i++){
           if(table.column(7).data().unique()[i] !== null){
             cuerpofechaCompDTH += '<option value="' + table.column(7).data().unique()[i] + '">' + table.column(7).data().unique()[i] + '</option>';
           }
         }
         $("#fechaCompOtDTH").html(cuerpofechaCompDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#fechaCompOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoRangoDTH = '';
         cuerpoRangoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(9).data().unique().length; i++){
           if(table.column(9).data().unique()[i] !== null){
             cuerpoRangoDTH += '<option value="' + table.column(9).data().unique()[i] + '">' + table.column(9).data().unique()[i] + '</option>';
           }
         }
         $("#rangoOtDTH").html(cuerpoRangoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#rangoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOtDTH = '';
         cuerpoOtDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(20).data().unique().length; i++){
           if(table.column(20).data().unique()[i] !== null){
             cuerpoOtDTH += '<option value="' + table.column(20).data().unique()[i] + '">' + table.column(20).data().unique()[i] + '</option>';
           }
         }
         $("#despachoOtDTH").html(cuerpoOtDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#despachoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoEstadoDTH = '';
         cuerpoEstadoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(5).data().unique().length; i++){
           if(table.column(5).data().unique()[i] !== null){
             cuerpoEstadoDTH += '<option value="' + table.column(5).data().unique()[i] + '">' + table.column(5).data().unique()[i] + '</option>';
           }
         }
         $("#estadoOtDTH").html(cuerpoEstadoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#estadoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoObsDTH = '';
         cuerpoObsDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(29).data().unique().length; i++){
           if(table.column(29).data().unique()[i] !== null){
             cuerpoObsDTH += '<option value="' + table.column(29).data().unique()[i] + '">' + table.column(29).data().unique()[i] + '</option>';
           }
         }
         $("#semafotoOtDTH").html(cuerpoObsDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#semafotoOtDTH").select2({
               theme: "bootstrap"
           });
         }
      }
    }
  });
});

$("#estadoOTDTH").unbind("click").click(async function(){
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  $("#tituloAsignarEstadoOTDTH").html("Cantidad de OT seleccionadas: " + datos.length);
  await $.ajax({
    url:   'controller/datosEstadoDTH.php',
    type:  'post',
    success: function (response2) {
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        var cuerpoDespachosDTH = '';
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoDespachosDTH += '<option value="' + p.aaData[i].ESTADO + '">' + p.aaData[i].ESTADO + '</option>';
        }
        $("#despachoAsignarEstadoOTDTH").html(cuerpoDespachosDTH);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#despachoAsignarEstadoOTDTH").select2({
        theme: "bootstrap"
    });
  }
  $("#modalAsignarEstadoOTDTH").modal("show");
});

$("#guardarAsignarEstadoOTDTH").unbind("click").click(function(){
  $("#modalAsignarEstadoOTDTH").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  arrayOtDth = [];
  for(var i = 0; i < datos.length; i++){
    arrayOtDth.push(datos[i].FOLIO.split("&nbsp;&nbsp;")[1]);
  }

  var parametros = {
    "estado": $("#despachoAsignarEstadoOTDTH").val(),
    "arrayOTDth": arrayOtDth
  }

  $.ajax({
    url:   'controller/actualizaEstadoOTDth.php',
    type:  'post',
    data:  parametros,
    success: async function (response) {
      var p = jQuery.parseJSON(response);
      if(p.aaData.length !== 0){
        var table = $('#tablaOtDTH').DataTable();
        var rows = table.rows( '.selected' ).remove().draw();
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Técnico asignado correctamente");
        $('#modalAlertas').modal('show');
        $("#agendarOTDTH").attr("disabled","disabled");
        $("#asignarDespachoOTDTH").attr("disabled","disabled");
        $("#asignarTecnicoOTDTH").attr("disabled","disabled");
        $("#estadoOTDTH").attr("disabled","disabled");
        $("#observacionOTDTH").attr("disabled","disabled");

        function ingresaModificacionesOTDth(p){
          var table2 = $('#tablaOtDTH').DataTable();
          for(var k = 0; k < p.aaData.length; k++){
            table2.rows.add([{
              'S': p.aaData[k].S,
              'FOLIO': p.aaData[k].FOLIO,
              'ORIGEN': p.aaData[k].ORIGEN,
              'TIPO_TICKET': p.aaData[k].TIPO_TICKET,
              'ESTADO_MANDANTE': p.aaData[k].ESTADO_MANDANTE,
              'ESTADO_DESPACHO': p.aaData[k].ESTADO_DESPACHO,
              'FECHA_CREACION': p.aaData[k].FECHA_CREACION,
              'FECHA_AGENDA': p.aaData[k].FECHA_AGENDA,
              'FECHA_GESTOR': p.aaData[k].FECHA_GESTOR,
              'RANGO': p.aaData[k].RANGO,
              'DIAS': p.aaData[k].DIAS,
              'DIAS_AGENDA': p.aaData[k].DIAS_AGENDA,
              'CANTIDAD_REAGENDAMIENTOS': p.aaData[k].CANTIDAD_REAGENDAMIENTOS,
              'COSTO': p.aaData[k].COSTO,
              'REGION': p.aaData[k].REGION,
              'COMUNA': p.aaData[k].COMUNA,
              'DIRECCION': p.aaData[k].DIRECCION,
              'RUT': p.aaData[k].RUT,
              'CLIENTE': p.aaData[k].CLIENTE,
              'FONO1': p.aaData[k].FONO1,
              'FONO2': p.aaData[k].FONO2,
              'DESPACHO': p.aaData[k].DESPACHO,
              'TECNICO': p.aaData[k].TECNICO,
              'OBSERVACION_DESPACHO': p.aaData[k].OBSERVACION_DESPACHO,
              'REFERENCIA': p.aaData[k].REFERENCIA,
              'REFERENCIA2': p.aaData[k].REFERENCIA2,
              'OBSERVACION_DESPACHO2': p.aaData[k].OBSERVACION_DESPACHO2,
              'HORA_AGENDA': p.aaData[k].HORA_AGENDA,
              'CALLE': p.aaData[k].CALLE,
              'NUMERO': p.aaData[k].NUMERO,
              'SEMAFORO': p.aaData[k].SEMAFORO
            }]).draw();
          }
        }

        await ingresaModificacionesOTDth(p);

        var filtro = '';

        table.columns('13').search(filtro).draw();
        table.columns('14').search(filtro).draw();
        table.columns('2').search(filtro).draw();
        table.columns('3').search(filtro).draw();
        table.columns('7').search(filtro).draw();
        table.columns('9').search(filtro).draw();
        table.columns('20').search(filtro).draw();
        table.columns('5').search(filtro).draw();
        table.columns('29').search(filtro).draw();


         var cuerpoRegionDTH = '';
         cuerpoRegionDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(13).data().unique().length; i++){
           if(table.column(13).data().unique()[i] !== null){
             cuerpoRegionDTH += '<option value="' + table.column(13).data().unique()[i] + '">' + table.column(13).data().unique()[i] + '</option>';
           }
         }
         $("#regionOtDTH").html(cuerpoRegionDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#regionOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoComunaDTH = '';
         cuerpoComunaDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(14).data().unique().length; i++){
           if(table.column(14).data().unique()[i] !== null){
             cuerpoComunaDTH += '<option value="' + table.column(14).data().unique()[i] + '">' + table.column(14).data().unique()[i] + '</option>';
           }
         }
         $("#comunaOtDTH").html(cuerpoComunaDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#comunaOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOrigenDTH = '';
         cuerpoOrigenDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(2).data().unique().length; i++){
           if(table.column(2).data().unique()[i] !== null){
             cuerpoOrigenDTH += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
           }
         }
         $("#origenOtDTH").html(cuerpoOrigenDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#origenOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoTTicketDTH = '';
         cuerpoTTicketDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(3).data().unique().length; i++){
           if(table.column(3).data().unique()[i] !== null){
             cuerpoTTicketDTH += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
           }
         }
         $("#tticketOtDTH").html(cuerpoTTicketDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#tticketOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpofechaCompDTH = '';
         cuerpofechaCompDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(7).data().unique().length; i++){
           if(table.column(7).data().unique()[i] !== null){
             cuerpofechaCompDTH += '<option value="' + table.column(7).data().unique()[i] + '">' + table.column(7).data().unique()[i] + '</option>';
           }
         }
         $("#fechaCompOtDTH").html(cuerpofechaCompDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#fechaCompOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoRangoDTH = '';
         cuerpoRangoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(9).data().unique().length; i++){
           if(table.column(9).data().unique()[i] !== null){
             cuerpoRangoDTH += '<option value="' + table.column(9).data().unique()[i] + '">' + table.column(9).data().unique()[i] + '</option>';
           }
         }
         $("#rangoOtDTH").html(cuerpoRangoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#rangoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOtDTH = '';
         cuerpoOtDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(20).data().unique().length; i++){
           if(table.column(20).data().unique()[i] !== null){
             cuerpoOtDTH += '<option value="' + table.column(20).data().unique()[i] + '">' + table.column(20).data().unique()[i] + '</option>';
           }
         }
         $("#despachoOtDTH").html(cuerpoOtDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#despachoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoEstadoDTH = '';
         cuerpoEstadoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(5).data().unique().length; i++){
           if(table.column(5).data().unique()[i] !== null){
             cuerpoEstadoDTH += '<option value="' + table.column(5).data().unique()[i] + '">' + table.column(5).data().unique()[i] + '</option>';
           }
         }
         $("#estadoOtDTH").html(cuerpoEstadoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#estadoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoObsDTH = '';
         cuerpoObsDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(29).data().unique().length; i++){
           if(table.column(29).data().unique()[i] !== null){
             cuerpoObsDTH += '<option value="' + table.column(29).data().unique()[i] + '">' + table.column(29).data().unique()[i] + '</option>';
           }
         }
         $("#semafotoOtDTH").html(cuerpoObsDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#semafotoOtDTH").select2({
               theme: "bootstrap"
           });
         }
      }
    }
  });
});

$("#agendarOTDTH").unbind("click").click(async function(){
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  $("#tituloAsignarAgendarOTDTH").html("OT seleccionada: " + datos[0].FOLIO);

  $('#fechaCompromisoAgendarOTDTH').val('');

  var min = moment().format('YYYY/MM/DD');
  $.datetimepicker.setLocale('es');
  $('#fechaCompromisoAgendarOTDTH').datetimepicker({
    format: 'd-m-Y H:i',
    minDate: min.toString(),
    step: 01
  });

  await $.ajax({
    url:   'controller/datosRango.php',
    type:  'post',
    success: function (response2) {
      var cuerpoRA = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          if(datos[0].RANGO === p.aaData[i].RANGO){
            cuerpoRA += "<option selected value='" + p.aaData[i].RANGO + "'>" + p.aaData[i].RANGO + "</option>";
          }
          else{
            cuerpoRA += "<option value='" + p.aaData[i].RANGO + "'>" + p.aaData[i].RANGO + "</option>";
          }
        }
        $("#rangoAgendarOTDTH").html(cuerpoRA);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#rangoAgendarOTDTH").select2({
        theme: "bootstrap"
    });
  }

  await $.ajax({
    url:   'controller/datosRegion.php',
    type:  'post',
    success: function (response2) {
      var cuerpoRE = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          if(datos[0].REGION === p.aaData[i].REGION){
            cuerpoRE += "<option selected value='" + p.aaData[i].REGION + "'>" + p.aaData[i].REGION + "</option>";
          }
          else{
            cuerpoRE += "<option value='" + p.aaData[i].REGION + "'>" + p.aaData[i].REGION + "</option>";
          }
        }
        $("#regionAgendarOTDTH").html(cuerpoRE);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#regionAgendarOTDTH").select2({
        theme: "bootstrap"
    });
  }

  var parRegion = {
    "region": $("#regionAgendarOTDTH").val()
  }

  await $.ajax({
    url:   'controller/datosComuna.php',
    type:  'post',
    data: parRegion,
    success: function (response2) {
      var cuerpoCO = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          if(datos[0].COMUNA === p.aaData[i].COMUNA){
            cuerpoCO += "<option selected value='" + p.aaData[i].COMUNA + "'>" + p.aaData[i].COMUNA + "</option>";
          }
          else{
            cuerpoCO += "<option value='" + p.aaData[i].COMUNA + "'>" + p.aaData[i].COMUNA + "</option>";
          }
        }
        $("#comunaAgendarOTDTH").html(cuerpoCO);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#comunaAgendarOTDTH").select2({
        theme: "bootstrap"
    });
  }

  var fc = datos[0].FECHA_AGENDA.split('-');
  // console.log(fc);
  var hc = datos[0].HORA_AGENDA.substring(0,5);

  var fc_ok = fc[2].toString() + '-' + fc[1].toString() + '-' + fc[0].toString() + ' ' + hc.toString();

  $("#fechaCompromisoAgendarOTDTH").val(fc_ok);
  $("#clienteAgendarOTDTH").val(datos[0].CLIENTE);
  $("#rutClienteAgendarOTDTH").val(datos[0].RUT);
  $("#calleAgendarOTDTH").val(datos[0].CALLE);
  $("#numeroAgendarOTDTH").val(datos[0].NUMERO);
  $("#fono1AgendarOTDTH").val(datos[0].FONO1);
  $("#fono2AgendarOTDTH").val(datos[0].FONO2);

  var h = $(window).height() - 200;
  $("#bodyAsignarAgendarOTDTH").css("height",h);
  setTimeout(function(){
    $('#modalAlertasSplash').modal('hide');
    $("#modalAsignarAgendarOTDTH").modal("show");
  },500);
});

$("#regionAgendarOTDTH").unbind("click").change(async function(){
  var parRegion = {
    "region": $("#regionAgendarOTDTH").val()
  }

  await $.ajax({
    url:   'controller/datosComuna.php',
    type:  'post',
    data: parRegion,
    success: function (response2) {
      var cuerpoCO = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoCO += "<option value='" + p.aaData[i].COMUNA + "'>" + p.aaData[i].COMUNA + "</option>";
        }
        $("#comunaAgendarOTDTH").html(cuerpoCO);
      }
    }
  });

  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#comunaAgendarOTDTH").select2('destroy').select2({
        theme: "bootstrap"
    });
  }
});

$("#guardarAsignarAgendarOTDTH").unbind("click").click(function(){
  $("#modalAsignarAgendarOTDTH").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  arrayOtDth = [];
  for(var i = 0; i < datos.length; i++){
    arrayOtDth.push(datos[i].FOLIO.split("&nbsp;&nbsp;")[1]);
  }

  var f = $("#fechaCompromisoAgendarOTDTH").val().split(' ');
  var fecha = f[0].split('-')[2].toString() + '-' + f[0].split('-')[1].toString() + '-' + f[0].split('-')[0].toString();
  var hora = f[1].toString();

  var parametros = {
    "fecha": fecha,
    "hora": hora,
    "rango": $("#rangoAgendarOTDTH").val(),
    "rut": $("#rutClienteAgendarOTDTH").val(),
    "cliente": $("#clienteAgendarOTDTH").val(),
    "region": $("#regionAgendarOTDTH").val(),
    "comuna": $("#comunaAgendarOTDTH").val(),
    "calle": $("#calleAgendarOTDTH").val(),
    "numero": $("#numeroAgendarOTDTH").val(),
    "fono1": $("#fono1AgendarOTDTH").val(),
    "fono2": $("#fono2AgendarOTDTH").val(),
    "arrayOTDth": arrayOtDth
  }

  $.ajax({
    url:   'controller/actualizaAgendaOTDTH.php',
    type:  'post',
    data:  parametros,
    success: async function (response) {
      var p = jQuery.parseJSON(response);
      if(p.aaData.length !== 0){
        var table = $('#tablaOtDTH').DataTable();
        var rows = table.rows( '.selected' ).remove().draw();
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Técnico asignado correctamente");
        $('#modalAlertas').modal('show');
        $("#agendarOTDTH").attr("disabled","disabled");
        $("#asignarDespachoOTDTH").attr("disabled","disabled");
        $("#asignarTecnicoOTDTH").attr("disabled","disabled");
        $("#estadoOTDTH").attr("disabled","disabled");
        $("#observacionOTDTH").attr("disabled","disabled");

        function ingresaModificacionesOTDth(p){
          var table2 = $('#tablaOtDTH').DataTable();
          for(var k = 0; k < p.aaData.length; k++){
            table2.rows.add([{
              'S': p.aaData[k].S,
              'FOLIO': p.aaData[k].FOLIO,
              'ORIGEN': p.aaData[k].ORIGEN,
              'TIPO_TICKET': p.aaData[k].TIPO_TICKET,
              'ESTADO_MANDANTE': p.aaData[k].ESTADO_MANDANTE,
              'ESTADO_DESPACHO': p.aaData[k].ESTADO_DESPACHO,
              'FECHA_CREACION': p.aaData[k].FECHA_CREACION,
              'FECHA_AGENDA': p.aaData[k].FECHA_AGENDA,
              'FECHA_GESTOR': p.aaData[k].FECHA_GESTOR,
              'RANGO': p.aaData[k].RANGO,
              'DIAS': p.aaData[k].DIAS,
              'DIAS_AGENDA': p.aaData[k].DIAS_AGENDA,
              'CANTIDAD_REAGENDAMIENTOS': p.aaData[k].CANTIDAD_REAGENDAMIENTOS,
              'COSTO': p.aaData[k].COSTO,
              'REGION': p.aaData[k].REGION,
              'COMUNA': p.aaData[k].COMUNA,
              'DIRECCION': p.aaData[k].DIRECCION,
              'RUT': p.aaData[k].RUT,
              'CLIENTE': p.aaData[k].CLIENTE,
              'FONO1': p.aaData[k].FONO1,
              'FONO2': p.aaData[k].FONO2,
              'DESPACHO': p.aaData[k].DESPACHO,
              'TECNICO': p.aaData[k].TECNICO,
              'OBSERVACION_DESPACHO': p.aaData[k].OBSERVACION_DESPACHO,
              'REFERENCIA': p.aaData[k].REFERENCIA,
              'REFERENCIA2': p.aaData[k].REFERENCIA2,
              'OBSERVACION_DESPACHO2': p.aaData[k].OBSERVACION_DESPACHO2,
              'HORA_AGENDA': p.aaData[k].HORA_AGENDA,
              'CALLE': p.aaData[k].CALLE,
              'NUMERO': p.aaData[k].NUMERO,
              'SEMAFORO': p.aaData[k].SEMAFORO
            }]).draw();
          }
        }

        await ingresaModificacionesOTDth(p);

        var filtro = '';

        table.columns('13').search(filtro).draw();
        table.columns('14').search(filtro).draw();
        table.columns('2').search(filtro).draw();
        table.columns('3').search(filtro).draw();
        table.columns('7').search(filtro).draw();
        table.columns('9').search(filtro).draw();
        table.columns('20').search(filtro).draw();
        table.columns('5').search(filtro).draw();
        table.columns('29').search(filtro).draw();


         var cuerpoRegionDTH = '';
         cuerpoRegionDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(13).data().unique().length; i++){
           if(table.column(13).data().unique()[i] !== null){
             cuerpoRegionDTH += '<option value="' + table.column(13).data().unique()[i] + '">' + table.column(13).data().unique()[i] + '</option>';
           }
         }
         $("#regionOtDTH").html(cuerpoRegionDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#regionOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoComunaDTH = '';
         cuerpoComunaDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(14).data().unique().length; i++){
           if(table.column(14).data().unique()[i] !== null){
             cuerpoComunaDTH += '<option value="' + table.column(14).data().unique()[i] + '">' + table.column(14).data().unique()[i] + '</option>';
           }
         }
         $("#comunaOtDTH").html(cuerpoComunaDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#comunaOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOrigenDTH = '';
         cuerpoOrigenDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(2).data().unique().length; i++){
           if(table.column(2).data().unique()[i] !== null){
             cuerpoOrigenDTH += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
           }
         }
         $("#origenOtDTH").html(cuerpoOrigenDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#origenOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoTTicketDTH = '';
         cuerpoTTicketDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(3).data().unique().length; i++){
           if(table.column(3).data().unique()[i] !== null){
             cuerpoTTicketDTH += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
           }
         }
         $("#tticketOtDTH").html(cuerpoTTicketDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#tticketOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpofechaCompDTH = '';
         cuerpofechaCompDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(7).data().unique().length; i++){
           if(table.column(7).data().unique()[i] !== null){
             cuerpofechaCompDTH += '<option value="' + table.column(7).data().unique()[i] + '">' + table.column(7).data().unique()[i] + '</option>';
           }
         }
         $("#fechaCompOtDTH").html(cuerpofechaCompDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#fechaCompOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoRangoDTH = '';
         cuerpoRangoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(9).data().unique().length; i++){
           if(table.column(9).data().unique()[i] !== null){
             cuerpoRangoDTH += '<option value="' + table.column(9).data().unique()[i] + '">' + table.column(9).data().unique()[i] + '</option>';
           }
         }
         $("#rangoOtDTH").html(cuerpoRangoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#rangoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoOtDTH = '';
         cuerpoOtDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(20).data().unique().length; i++){
           if(table.column(20).data().unique()[i] !== null){
             cuerpoOtDTH += '<option value="' + table.column(20).data().unique()[i] + '">' + table.column(20).data().unique()[i] + '</option>';
           }
         }
         $("#despachoOtDTH").html(cuerpoOtDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#despachoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoEstadoDTH = '';
         cuerpoEstadoDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(5).data().unique().length; i++){
           if(table.column(5).data().unique()[i] !== null){
             cuerpoEstadoDTH += '<option value="' + table.column(5).data().unique()[i] + '">' + table.column(5).data().unique()[i] + '</option>';
           }
         }
         $("#estadoOtDTH").html(cuerpoEstadoDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#estadoOtDTH").select2('destroy').select2({
               theme: "bootstrap"
           });
         }

         var cuerpoObsDTH = '';
         cuerpoObsDTH += '<option selected value="Todos">Todos</option>';
         for(var i = 0; i < table.column(29).data().unique().length; i++){
           if(table.column(29).data().unique()[i] !== null){
             cuerpoObsDTH += '<option value="' + table.column(29).data().unique()[i] + '">' + table.column(29).data().unique()[i] + '</option>';
           }
         }
         $("#semafotoOtDTH").html(cuerpoObsDTH);
         if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           $("#semafotoOtDTH").select2({
               theme: "bootstrap"
           });
         }
      }
    }
  });
});

// Usuarios
$("#agregarUsuario").unbind("click").click(async function(){
  $("#modalIngresoUsuario").find("input,textarea,select").val("");
  $("#rutIngresoUsuario").removeClass("is-invalid");
  $("#apellidosIngresoUsuario").removeClass("is-invalid");
  $("#nombresIngresoUsuario").removeClass("is-invalid");
  $("#emailUsuario").removeClass("is-invalid");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  await $.ajax({
    url:   'controller/datosPerfilTipos.php',
    type:  'post',
    success: function (response2) {
      var p2 = jQuery.parseJSON(response2);
      if(p2.aaData.length !== 0){
        var cuerpoEC = '';
        for(var i = 0; i < p2.aaData.length; i++){
          cuerpoEC += '<option value="' + p2.aaData[i].IDPERFIL + '">' + p2.aaData[i].NOMBRE + '</option>';
        }
        $("#perfilUsuario").html(cuerpoEC);
      }
    }
  });
  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#perfilUsuario").select2({
        theme: "bootstrap"
    });
  }
  setTimeout(function(){
    $("#modalIngresoUsuario").modal("show");
    $('#modalAlertasSplash').modal('hide');
    setTimeout(function(){
      $('#bodyIngresoUsuario').animate({ scrollTop: 0 }, 'fast');
    },200);
  },500);
});

$("input#rutIngresoUsuario").rut({
  formatOn: 'keyup',
  minimumLength: 8,
  validateOn: 'change'
}).on('rutInvalido', function(e) {
  if($("#rutIngresoUsuario").val() !== ''){
    $("#buttonAceptarAlerta").css("display","inline");
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("El rut ingresado no es válido");
    $('#modalAlertas').modal('show');
    $("#rutIngresoUsuario").val("");
    $("#rutIngresoUsuario").addClass("is-invalid");
  }
});

$("#guardarIngresoUsuario").unbind('click').click(function(){
  if($("#rutIngresoUsuario").val().length == 0 || $("#apellidosIngresoUsuario").val().length == 0 || $("#nombresIngresoUsuario").val().length == 0 || $("#emailUsuario").val().length == 0){
    $("#buttonAceptarAlerta").css("display","inline");
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("Debe completar todos los campos");
    if ($("#rutIngresoUsuario").val().length == 0){
      $("#rutIngresoUsuario").addClass("is-invalid");
    }
    else if ($("#apellidosIngresoUsuario").val().length == 0){
      $("#apellidosIngresoUsuario").addClass("is-invalid");
    }
    else if ($("#nombresIngresoUsuario").val().length == 0){
      $("#nombresIngresoUsuario").addClass("is-invalid");
    }
    else if ($("#emailUsuario").val().length == 0){
      $("#emailUsuario").addClass("is-invalid");
    }
  }
  else {
  parametros = {
    "rutIngreso":   $.trim($("#rutIngresoUsuario").val().replace('.','').replace('.','')),
    "apellidos":  $("#apellidosIngresoUsuario").val(),
    "nombres": $("#nombresIngresoUsuario").val(),
    "email": $("#emailUsuario").val(),
    "perfilUsuario": $("#perfilUsuario").val(),
    "passUsuario": randomTexto()
  }
  //console.log(parametros);
  $.ajax({
      url:   'controller/datosChequeaUsuario.php',
      type:  'post',
      data:  parametros,
      success:  function (response) {
        var p = response.split(",");
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            $("#buttonAceptarAlerta").css("display","inline");
            $("#textoModal").html("El rut del usuario ya existe");
            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $('#modalAlertas').modal('show');
          }
        }
        else{
          $.ajax({
            url:   'controller/datosChequeaEmail.php',
            type:  'post',
            data:  parametros,
            success:  function (response) {
              var p = response.split(",");
              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                  $("#buttonAceptarAlerta").css("display","inline");
                  $("#textoModal").html("Ya existe un usuario creado con ese email");
                  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                  $('#modalAlertas').modal('show');
                }
              }
              else {
                $("#modalIngresoUsuario").modal("hide");
                $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
                $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
                $('#modalAlertasSplash').modal('show');
                $.ajax({
                  url: "controller/ingresaUsuario.php",
                  type: 'POST',
                  data: parametros,
                  success:  function (response) {
                    var p = response.split(",");
                    if(response.localeCompare("Sin datos")!= 0 && response != ""){
                      if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                        var table = $('#tablaListadoUsuarios').DataTable();
                        //table.rows('.selected').remove().draw();
                        table.ajax.reload();
                        $("#buttonAceptarAlerta").css("display","inline");
                        var random = Math.round(Math.random() * (1000000 - 1) + 1);
                        $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Usuario creado correctamente");
                        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                        $('#modalAlertas').modal('show');
                        $("#editarUsuario").attr("disabled","disabled");
                        $("#usuarioResetPass").attr("disabled","disabled");

                      }
                      else{
                        $("#buttonAceptarAlerta").css("display","inline");
                        var random = Math.round(Math.random() * (1000000 - 1) + 1);
                        $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al crear usuario, si el problema persiste favor comuniquese con soporte");
                        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                        $('#modalAlertas').modal('show');
                      }
                    }
                    else{
                      $("#buttonAceptarAlerta").css("display","inline");
                      var random = Math.round(Math.random() * (1000000 - 1) + 1);
                      $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al crear usuario, si el problema persiste favor comuniquese con soporte");
                      $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                      $('#modalAlertas').modal('show');
                    }
                  }
                });
              }
            }
          });
        }
      }
    });
  }
});

$("#rutIngresoUsuario").on('input', function(){
  $(this).removeClass("is-invalid");
});

$("#apellidosIngresoUsuario").on('input', function(){
  $(this).removeClass("is-invalid");
});

$("#nombresIngresoUsuario").on('input', function(){
  $(this).removeClass("is-invalid");
});

$("#emailUsuario").unbind('click').click(function(){
  $(this).removeClass("is-invalid");
});

$("#desactivarUsuario").unbind('click').click(function(){
  var table = $('#tablaListadoUsuarios').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.RUT;
  });
  var nombre = $.map(table.rows('.selected').data(), function (item) {
      return item.NOMBRE;
  });
  $("#textoDesactivarUsuario").html(rutUsuario[0] + ' , ' +nombre[0]);
  $('#modalDesactivarUsuario').modal('show');
});

$("#guardarDesactivarUsuario").unbind('click').click(async function(){
  var table = $('#tablaListadoUsuarios').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.RUT;
  });

  parametros = {
    "rutUsuario": rutUsuario[0]
  }
  //console.log(parametros);
  await $.ajax({
    url:   'controller/desactivarUsuario.php',
    type:  'post',
    data:  parametros,
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          var table = $('#tablaListadoUsuarios').DataTable();
          //table.rows('.selected').remove().draw();
          table.ajax.reload()
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Usuario desactivado correctamente");
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          $('#modalAlertas').modal('show');
          $("#editarUsuario").attr("disabled","disabled");
          $("#usuarioResetPass").attr("disabled","disabled");

        }
        else{
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al desactivar al usuario, si el problema persiste favor comuniquese con soporte");
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          $('#modalAlertas').modal('show');
        }
      }
      else{
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al desactivar al usuario, si el problema persiste favor comuniquese con soporte");
        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
        $('#modalAlertas').modal('show');
      }
    }
  });
  $('#modalDesactivarUsuario').modal('hide');
});

$("#editarUsuario").unbind('click').click(function(){
  var table = $('#tablaListadoUsuarios').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.RUT;
  });
  var nombre = $.map(table.rows('.selected').data(), function (item) {
      return item.NOMBRE;
  });
  var email = $.map(table.rows('.selected').data(), function (item) {
      return item.EMAIL;
  });
  var perfil = $.map(table.rows('.selected').data(), function (item) {
      return item.PERFIL;
  });
  var estado = $.map(table.rows('.selected').data(), function (item) {
      return item.ESTADO;
  });
  $("#rutEditarUsuario").val(rutUsuario);
  $("#nombreEditarUsuario").val(nombre);
  $("#emailEditarUsuario").val(email);
  $("#perfilEditarUsuario").val(perfil);
  $("#estadoEditarUsuario").val(estado);
  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#estadoEditarUsuario").select2({
        theme: "bootstrap"
    });
  }

  $('#modalEditarUsuario').modal('show');
});

$("input#rutEditarUsuario").rut({
  formatOn: 'keyup',
  minimumLength: 8,
  validateOn: 'change'
}).on('rutInvalido', function(e) {
  if($("#rutIngresoUsuario").val() !== ''){
    $("#buttonAceptarAlerta").css("display","inline");
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("El rut ingresado no es válido");
    $('#modalAlertas').modal('show');
    //$("#rutIngresoUsuario").val("");
  }
});

$("#guardarEditarUsuario").unbind('click').click(async function(){
  $("#modalEditarUsuario").modal("hide");
  parametros = {
    "rutEditar":   $("#rutEditarUsuario").val(),
    "nombre":  $("#nombreEditarUsuario").val(),
    "email": $("#emailEditarUsuario").val(),
    "estado": $("#estadoEditarUsuario").val()
    //"perfilUsuario": $("#perfilEditarUsuario").val()
  }
  //console.log(parametros);
  await $.ajax({
    url: "controller/editarUsuario.php",
    type: 'POST',
    data: parametros,
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){

          $('#modalAlertas').modal({backdrop: 'static', keyboard: false});
          var table = $('#tablaListadoUsuarios').DataTable();
          table.ajax.reload();
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Usuario editado correctamente");
          $('#modalAlertas').modal('show');
          $("#editarUsuario").attr("disabled","disabled");
          $("#usuarioResetPass").attr("disabled","disabled");

        }
        else{
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al editar al usuario, si el problema persiste favor comuniquese con soporte");
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          $('#modalAlertas').modal('show');
        }
      }
      else{
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al editar al usuario, si el problema persiste favor comuniquese con soporte");
        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
        $('#modalAlertas').modal('show');
      }
    }
  });
});

$("#usuarioResetPass").unbind('click').click(async function(){
  var table = $('#tablaListadoUsuarios').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.RUT;
  });
  var nombre = $.map(table.rows('.selected').data(), function (item) {
      return item.NOMBRE;
  });
  $("#tituloDesvincular").html(rutUsuario[0] + ' , ' +nombre[0]);
  $('#modalResetPassUsuario').modal('show');
});

$("#guardarResetPassUsuario").unbind('click').click(async function(){
  $('#modalResetPassUsuario').modal('hide');
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaListadoUsuarios').DataTable();
  var rutUsuario = $.map(table.rows('.selected').data(), function (item) {
      return item.RUT;
  });
  var nombre = $.map(table.rows('.selected').data(), function (item) {
      return item.NOMBRE;
  });
  var email = $.map(table.rows('.selected').data(), function (item) {
      return item.EMAIL;
  });

  parametros = {
    "rutUsuario": rutUsuario[0],
    "nombreUsuario": nombre[0],
    "mailUsuario": email[0],
    "passUsuario": randomTexto()
  }
  console.log(parametros);
  await $.ajax({
    url:   'controller/resetPass.php',
    type:  'post',
    data:  parametros,
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          var table = $('#tablaListadoUsuarios').DataTable();
          //table.rows('.selected').remove().draw();
          table.ajax.reload()
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Contraseña reseteada correctamente");
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          $('#modalAlertas').modal('show');
          $("#editarUsuario").attr("disabled","disabled");
          $("#usuarioResetPass").attr("disabled","disabled");

        }
        else{
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al resetear contraseña, si el problema persiste favor comuniquese con soporte");
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          $('#modalAlertas').modal('show');
        }
      }
      else{
        $("#buttonAceptarAlerta").css("display","inline");
        var random = Math.round(Math.random() * (1000000 - 1) + 1);
        $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Error al resetear contraseña, si el problema persiste favor comuniquese con soporte");
        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
        $('#modalAlertas').modal('show');
      }
    }
  });
});

function randomTexto(){
  var str = '';
  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
  for (var i = 0; i < 8 ; i++)
  {
    str += ref.charAt(Math.floor(Math.random()*ref.length));
  }
  return str;
}

function addCambiaPass(){
    $("#guardarChangePass").unbind();
    $("#guardarChangePass").unbind('click').click(function(){
        var pass1 = $("#passNuevo").val();
        var pass2 = $("#passNuevoConfirmar").val();
        if(pass1 == "" && pass2 == "")
        {
            $("#passNuevo").addClass("is-invalid");
            $("#passNuevoConfirmar").addClass("is-invalid");

            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#buttonAceptarAlerta").css("display","inline");
            var random = Math.round(Math.random() * (1000000 - 1) + 1);
            $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Ingrese todos los campos");
            $('#modalAlertas').modal('show');
        }
        else if(pass1 == ""){
            $("#passNuevo").addClass("is-invalid");

            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#buttonAceptarAlerta").css("display","inline");
            var random = Math.round(Math.random() * (1000000 - 1) + 1);
            $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Ingrese todos los campos");
            $('#modalAlertas').modal('show');
        }
        else if(pass2 == ""){
            $("#passNuevoConfirmar").addClass("is-invalid");

            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#buttonAceptarAlerta").css("display","inline");
            var random = Math.round(Math.random() * (1000000 - 1) + 1);
            $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Ingrese todos los campos");
            $('#modalAlertas').modal('show');
        }
        else if(pass1 == pass2){
            var parametros = {
                "pass" :  $("#passNuevo").val()
            };
            //Chequeo mismo pass
            $.ajax({
                data:  parametros,
                url:   'controller/checkPassIgual.php',
                type:  'post',
                success:  function (response) {
                    var p = response.split(",");
                    if(p != null){
                        if(p[0] != "Sin datos" && p[0] != "" && p[0] != 'Error'){
                            if(p[0] == "Igual"){
                                $("#passNuevo").addClass("is-invalid");
                                $("#passNuevoConfirmar").addClass("is-invalid");

                                $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                                $("#buttonAceptarAlerta").css("display","inline");
                                var random = Math.round(Math.random() * (1000000 - 1) + 1);
                                $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Contraseña igual a la actual");
                                $('#modalAlertas').modal('show');
                            }
                            else{
                                $.ajax({
                                  data:  parametros,
                                  url:   'controller/actualizaPass.php',
                                  type:  'post',
                                  beforeSend: function(){
                                      $("#modalChangePass").modal("hide");
                                      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
                                      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
                                      $('#modalAlertasSplash').modal('show');
                                  },
                                  success:  function (response) {
                                      var p = response.split(",");
                                      if(p != null){
                                          if(p[0] != "Sin datos" && p[0] != "" && p[0] != 'Error'){
                                              $("#buttonAceptarAlerta").css("display","inline");
                                              $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
                                              var random = Math.round(Math.random() * (1000000 - 1) + 1);
                                              $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Contraseña actualizada correctamente");
                                              $('#modalAlertasSplash').modal('show');
                                              $("#passNuevo").removeClass("is-invalid");
                                              $("#passNuevoConfirmar").removeClass("is-invalid");
                                              $("#passNuevo").val("");
                                              $("#passNuevoConfirmar").val("");
                                              $("#mensajeCambioPass").hide();
                                              $.ajax({
                                                  url:   'controller/cerraSesion.php',
                                                  type:  'post',
                                                  success: function (response) {
                                                    $(".contenedor-logos").css("display","none");
                                                    $(".contenedor-logos").find('li').css("display","none");
                                                    $("#sesionActiva").val("0");
                                                    $("#sesionActivaUso").val("0");
                                                    $("#logoLinkWeb").fadeOut();
                                                    $("#logoMenu").fadeOut();
                                                    $("#lineaMenu").fadeOut();
                                                    $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                                                    $("#menu-lateral").css("width","45px");
                                                    $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                                                    $("#logoMenu").css("color","black");
                                                    $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                                                    $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                                                    $("#DivPrincipalMenu").empty();

                                                    window.location.href = "#/home";
                                                  }
                                              });
                                          }
                                          else{
                                            $("#buttonAceptarAlerta").css("display","inline");
                                            var random = Math.round(Math.random() * (1000000 - 1) + 1);
                                            $("#textoModal").html("<img src='view/img/error.gif?" + random +  "' class='splash_load'><br/>Error en la actualización si el problema persiste<br/>favor comuniquese con soporte");
                                            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                                            $('#modalAlertas').modal('show');
                                          }
                                      }
                                      else{
                                        $("#buttonAceptarAlerta").css("display","inline");
                                        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                                        var random = Math.round(Math.random() * (1000000 - 1) + 1);
                                        $("#textoModal").html("<img src='view/img/error.gif?" + random +  "' class='splash_load'><br/>Error en la actualización si el problema persiste<br/>favor comuniquese con soporte");
                                        $('#modalAlertas').modal('show');
                                      }
                                    }
                                  });
                            }
                        }
                    }
                }
            });
        }
        else{
            $("#passNuevo").addClass("is-invalid");
            $("#passNuevoConfirmar").addClass("is-invalid");

            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#buttonAceptarAlerta").css("display","inline");
            var random = Math.round(Math.random() * (1000000 - 1) + 1);
            $("#textoModal").html("<img src='view/img/error.gif?" + random +  "'  class='splash_load'><br/>Las contraseñas no coinciden");
            $('#modalAlertas').modal('show');
        }
    });
}

$("#passNuevo").on('input', function(){
  $(this).removeClass("is-invalid");
});

$("#passNuevoConfirmar").on('input', function(){
  $(this).removeClass("is-invalid");
});

$("#fechaLina").unbind("click").change(function(){
  clearInterval(lineaTiempo);

  dibujaTimeline($("#fechaLina").val());

  lineaTiempo = setInterval(function (e) {
    dibujaTimeline($("#fechaLina").val());
    e.preventDefault();
    e.stopImmediatePropagation();
  }, 60000);

  var element = document.getElementById('contenido');
  new ResizeSensor(element, function(e) {
      dibujaTimeline($("#fechaLina").val());
      e.preventDefault();
      e.stopImmediatePropagation();
  });
});

function dibujaTimeline(fecha){
  google.charts.load('current', {'packages':['timeline'], 'language': 'es'});
  google.charts.setOnLoadCallback(drawChart);

  var tiempo;

  if(fecha === moment().format('YYYY-MM-DD').toString()){
    momentoActual = new Date();
    hora = momentoActual.getHours();
    minuto = momentoActual.getMinutes();
    segundo = momentoActual.getSeconds();

    if(hora < 10){
      hora = '0' + hora;
    }
    if(minuto < 10){
      minuto = '0' + minuto;
    }
    if(segundo < 10){
      segundo = '0' + segundo;
    }
    tiempo = hora + ":" + minuto;
  }
  else{
    hora = '23';
    minuto = '59';
    segundo = '59';
    tiempo = '23:59';
  }

  function MarcarHoy (div, filaActual){
    var altura = 0;
    $('#'+div+' rect').each(function( index ) {
      yValor = parseFloat($(this).attr('y'));
      xValor = parseFloat($(this).attr('x'));
      if ( yValor == 0 && yValor == 0 ) { altura = parseFloat($(this).attr('height')) };
    });
    $('#'+div+' text:contains(' + tiempo +')').css('font-size','11px').attr('fill','#A6373C').prev().first().attr('height',altura+'px').attr('width','1px').attr('y','0');

    $('#'+div+' text:contains("Hora actual")').css('font-size','11px').attr('fill','#A6373C').prev().first().attr('y','0');

    if (filaActual != -1) {
      if ( 0 == filaActual )
        $('.google-visualization-tooltip').css('display','none');
      else
        $('.google-visualization-tooltip').css('display','inline');
    }
  }

  async function drawChart() {
    var container = document.getElementById('timeline');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();

    dataTable.addColumn({ type: 'string', id: 'TAC' });
    dataTable.addColumn({ type: 'string', id: 'Tipo' });
    dataTable.addColumn({ type: 'string', id: 'style', role: 'style' });
    dataTable.addColumn({ type: 'date', id: 'Inicio' });
    dataTable.addColumn({ type: 'date', id: 'Fin' });
    dataTable.addColumn({ type: 'string', role: 'tooltip', id: 'Id' });

    dataTable.addRows([
      ['Hora actual', tiempo, 'red', new Date(0,0,0,hora,minuto,segundo),  new Date(0,0,0,hora,minuto,segundo), 'hoy'],
    ]);

    var parametros = {
      "fecha": fecha
    }

    await $.ajax({
      url:   'controller/datosDTHLineaTiempo.php',
      type:  'post',
      data: parametros,
      success: function (response2) {
        var p2 = jQuery.parseJSON(response2);
        if(p2.aaData.length !== 0){
          for(var i = 0; i < p2.aaData.length; i++){
            dataTable.addRows([[
              p2.aaData[i].NOMBRE,
              p2.aaData[i].TIPO_TICKET,
              p2.aaData[i].COLOR,
              new Date(0,0,0,p2.aaData[i].HORA_INICIO,p2.aaData[i].MIN_INICIO,p2.aaData[i].SEC_INICIO),
              new Date(0,0,0,p2.aaData[i].HORA_FIN,p2.aaData[i].MIN_FIN,p2.aaData[i].SEC_FIN),
              p2.aaData[i].FOLIO
            ]]);
          }
        }
      }
    });

    var options = {
      timeline: {
        rowLabelStyle: {
          fontSize: 14,
          color: 'black'
        }
      },
      avoidOverlappingGridLines: false,
      enableInteractivity: true
    };

    chart.draw(dataTable, options);
    MarcarHoy('timeline',-1);

    google.visualization.events.addListener(chart, 'onmouseover', function(obj) {
      MarcarHoy('timeline',obj.row);
    });

    google.visualization.events.addListener(chart, 'onmouseout', function(obj) {
      MarcarHoy('timeline',-1);
    });

    google.visualization.events.addListener(chart, 'select', function () {
      var selection = chart.getSelection();
      if (selection.length > 0 && dataTable.getValue(selection[0].row, 5) !== 'hoy') {
        alert("OT: " + dataTable.getValue(selection[0].row, 5));
      }
      chart.clearChart();
      chart.draw(dataTable, options);
      MarcarHoy('timeline',-1);
    });
  }
}

$("#fechaTacLina").unbind("click").change(function(){
  clearInterval(lineaTacTiempo);

  dibujaOTDthTac($("#fechaTacLina").val());

  lineaTacTiempo = setInterval(function (e) {
    dibujaOTDthTac($("#fechaTacLina").val());
    e.preventDefault();
    e.stopImmediatePropagation();
  }, 60000);

  var element = document.getElementById('contenido');
  new ResizeSensor(element, function(e) {
      dibujaOTDthTac($("#fechaTacLina").val());
      e.preventDefault();
      e.stopImmediatePropagation();
  });
});

async function dibujaOTDthTac(fecha){
  var parametros = {
    "fecha": fecha
  }
  await $.ajax({
    url:   'controller/datosDTHLineaTiempoTac.php',
    type:  'post',
    data: parametros,
    success: function (response2) {
      var p2 = jQuery.parseJSON(response2);
      if(p2.aaData.length !== 0){
        var htmlCuerpo = '';
        for(var i = 0; i < p2.aaData.length; i++){
          if(p2.aaData[i].HORA_INICIO < 10){
            p2.aaData[i].HORA_INICIO = '0' + p2.aaData[i].HORA_INICIO
          }
          if(p2.aaData[i].MIN_INICIO < 10){
            p2.aaData[i].MIN_INICIO = '0' + p2.aaData[i].MIN_INICIO
          }
          if(p2.aaData[i].HORA_FIN < 10){
            p2.aaData[i].HORA_FIN = '0' + p2.aaData[i].HORA_FIN
          }
          if(p2.aaData[i].MIN_FIN < 10){
            p2.aaData[i].MIN_FIN = '0' + p2.aaData[i].MIN_FIN
          }

          if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            htmlCuerpo += `<div style="padding-top: 10px; margin-bottom: 15px; border: 1px solid black; background-color: ${p2.aaData[i].COLOR}; padding-bottom: 10px;; font-weight: bold; padding-left: 10px;">
              ${i+1}.-&nbsp;&nbsp;${p2.aaData[i].COMUNA}&nbsp;-&nbsp;${p2.aaData[i].TIPO_TICKET}<br>OT:&nbsp;${p2.aaData[i].FOLIO}&nbsp;&nbsp;Rango:&nbsp;${p2.aaData[i].HORA_INICIO}:${p2.aaData[i].MIN_INICIO}&nbsp;-&nbsp;${p2.aaData[i].HORA_FIN}:${p2.aaData[i].MIN_FIN}
            </div>`;
          }
          else{
            htmlCuerpo += `<div style="padding-top: 10px; margin-bottom: 15px; border: 1px solid black; background-color: ${p2.aaData[i].COLOR}; font-weight: bold; padding-bottom: 10px; padding-left: 10px;">
              ${i+1}.-&nbsp;&nbsp;${p2.aaData[i].COMUNA}&nbsp;-&nbsp;${p2.aaData[i].TIPO_TICKET}<br>OT:&nbsp;${p2.aaData[i].FOLIO}&nbsp;&nbsp;Rango:&nbsp;${p2.aaData[i].HORA_INICIO}:${p2.aaData[i].MIN_INICIO}&nbsp;-&nbsp;${p2.aaData[i].HORA_FIN}:${p2.aaData[i].MIN_FIN}
            </div>`;
          }
          // p2.aaData[i].NOMBRE,
          // p2.aaData[i].TIPO_TICKET,
          // p2.aaData[i].COLOR,
          // new Date(0,0,0,p2.aaData[i].HORA_INICIO,p2.aaData[i].MIN_INICIO,p2.aaData[i].SEC_INICIO),
          // new Date(0,0,0,p2.aaData[i].HORA_FIN,p2.aaData[i].MIN_FIN,p2.aaData[i].SEC_FIN),
          // p2.aaData[i].FOLIO


        }
        $("#lineaTac").html(htmlCuerpo);

        generaMultiMapaTac(p2);
      }
    }
  });
}

async function generaMultiMapaTac(p){
	u = 'https://maps.googleapis.com/maps/api/geocode/json?';

  for(var i = 0; i < p.aaData.length; i++){
    datos = {
    		address	: p.aaData[i].CALLE + ' ' + p.aaData[i].NUMERO + ', ' + p.aaData[i].COMUNA + ', Chile',
  		key		: 'AIzaSyCb15rFHKSkFxmZbQIo6KVes2-GR3N-LcQ'
  	};

  	await $.ajax({
  		url: u,
  		type: 'get',
  		dataType: "json",
  		data: datos,
  		success: function(data){
        p.aaData[i].lat = data.results[0].geometry.location.lat;
  			p.aaData[i].lng = data.results[0].geometry.location.lng;
  		}
  	});
  }

  google.charts.load("current", {
    "packages":["map"],
    "mapsApiKey": "AIzaSyCb15rFHKSkFxmZbQIo6KVes2-GR3N-LcQ"
  });

  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = new google.visualization.DataTable();

    data.addColumn({ type: 'number', id: 'Lat' });
    data.addColumn({ type: 'number', id: 'Long' });
    data.addColumn({ type: 'string', id: 'Name' });
    data.addColumn({ type: 'string', id: 'Marker' });

    for(var i = 0; i < p.aaData.length; i++){
      data.addRows([[
        p.aaData[i].lat,
        p.aaData[i].lng,
        p.aaData[i].CALLE + ' ' + p.aaData[i].NUMERO + ', ' + p.aaData[i].COMUNA + ', Chile',
        (i+1).toString() + p.aaData[i].ESTADO_DESPACHO
      ]]);
    }

    var options = {
      zoomLevel: 12,
      showTooltip: true,
      showInfoWindow: true,
      mapType: 'terrain',
      icons: {
        '1Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|ffb600|000000'
        },
        '2Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|ffb600|000000'
        },
        '3Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|ffb600|000000'
        },
        '4Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|ffb600|000000'
        },
        '5Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|ffb600|000000'
        },
        '6Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|ffb600|000000'
        },
        '7Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|ffb600|000000'
        },
        '8Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|ffb600|000000'
        },
        '9Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|ffb600|000000'
        },
        '10Asignada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|ffb600|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|ffb600|000000'
        },
        '1Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|00b9ff|000000'
        },
        '2Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|00b9ff|000000'
        },
        '3Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|00b9ff|000000'
        },
        '4Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|00b9ff|000000'
        },
        '5Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|00b9ff|000000'
        },
        '6Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|00b9ff|000000'
        },
        '7Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|00b9ff|000000'
        },
        '8Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|00b9ff|000000'
        },
        '9Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|00b9ff|000000'
        },
        '10Despachada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|00b9ff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|00b9ff|000000'
        },
        '1En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|007cff|000000'
        },
        '2En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|007cff|000000'
        },
        '3En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|007cff|000000'
        },
        '4En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|007cff|000000'
        },
        '5En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|007cff|000000'
        },
        '6En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|007cff|000000'
        },
        '7En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|007cff|000000'
        },
        '8En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|007cff|000000'
        },
        '9En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|007cff|000000'
        },
        '10En trabajo': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|007cff|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|007cff|000000'
        },
        '1Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|17ff00|000000'
        },
        '2Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=2|17ff00|000000'
        },
        '3Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|17ff00|000000'
        },
        '4Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|17ff00|000000'
        },
        '5Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=5|17ff00|000000'
        },
        '6Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=6|17ff00|000000'
        },
        '7Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=7|17ff00|000000'
        },
        '8Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=8|17ff00|000000'
        },
        '9Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=9|17ff00|000000'
        },
        '10Liquidada': {
          normal:   'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|17ff00|000000',
          selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=10|17ff00|000000'
        }
      }

    };

    setTimeout(function(){
      $("#mapaTacContenido").width($("#mapaTac").width());
      $("#footer").css("margin-top",50);
      $("#footer").css("margin-bottom",25);
      $("#btnWsspImg").css("display", "none");
      var map = new google.visualization.Map(document.getElementById('mapaTacContenido'));
      map.draw(data, options);
    },200);
  }
}

$("#observacionOTDTH").unbind("click").click(function(){
  $("#observacionDTHIng").val('');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();
  $("#tituloIngrObsDTH").html("OT seleccionada: " + datos[0].FOLIO);
  $("#observacionDTHIng").val(datos[0].OBSERVACION_DESPACHO2);

  $("#modalObsDTH").modal("show");
});

$("#guardarObsDTH").unbind("click").click(function(){
  $("#modalObsDTH").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var table = $('#tablaOtDTH').DataTable();
  var datos = table.rows('.selected').data();

  arrayOtDth = [];
  for(var i = 0; i < datos.length; i++){
    arrayOtDth.push(datos[i].FOLIO.split("&nbsp;&nbsp;")[1]);
  }

  var parametros = {
    "observacion": $("#observacionDTHIng").val(),
    "arrayOtDth": arrayOtDth
  }

  $.ajax({
      url:   'controller/actualizaObservacionOTDth.php',
      type:  'post',
      data:  parametros,
      success: async function (response) {
        var p = jQuery.parseJSON(response);
        if(p.aaData.length !== 0){
          var table = $('#tablaOtDTH').DataTable();
          var rows = table.rows( '.selected' ).remove().draw();
          $("#buttonAceptarAlerta").css("display","inline");
          var random = Math.round(Math.random() * (1000000 - 1) + 1);
          $("#textoModal").html("<img src='view/img/check.gif?" + random +  "'  class='splash_load'><br/>Despacho asignado correctamente");
          $('#modalAlertas').modal('show');
          $("#agendarOTDTH").attr("disabled","disabled");
          $("#asignarDespachoOTDTH").attr("disabled","disabled");
          $("#asignarTecnicoOTDTH").attr("disabled","disabled");
          $("#estadoOTDTH").attr("disabled","disabled");
          $("#observacionOTDTH").attr("disabled","disabled");

          function ingresaModificacionesOTDth(p){
            var table2 = $('#tablaOtDTH').DataTable();
            for(var k = 0; k < p.aaData.length; k++){
              table2.rows.add([{
                'S': p.aaData[k].S,
                'FOLIO': p.aaData[k].FOLIO,
                'ORIGEN': p.aaData[k].ORIGEN,
                'TIPO_TICKET': p.aaData[k].TIPO_TICKET,
                'ESTADO_MANDANTE': p.aaData[k].ESTADO_MANDANTE,
                'ESTADO_DESPACHO': p.aaData[k].ESTADO_DESPACHO,
                'FECHA_CREACION': p.aaData[k].FECHA_CREACION,
                'FECHA_AGENDA': p.aaData[k].FECHA_AGENDA,
                'FECHA_GESTOR': p.aaData[k].FECHA_GESTOR,
                'RANGO': p.aaData[k].RANGO,
                'DIAS': p.aaData[k].DIAS,
                'DIAS_AGENDA': p.aaData[k].DIAS_AGENDA,
                'CANTIDAD_REAGENDAMIENTOS': p.aaData[k].CANTIDAD_REAGENDAMIENTOS,
                'COSTO': p.aaData[k].COSTO,
                'REGION': p.aaData[k].REGION,
                'COMUNA': p.aaData[k].COMUNA,
                'DIRECCION': p.aaData[k].DIRECCION,
                'RUT': p.aaData[k].RUT,
                'CLIENTE': p.aaData[k].CLIENTE,
                'FONO1': p.aaData[k].FONO1,
                'FONO2': p.aaData[k].FONO2,
                'DESPACHO': p.aaData[k].DESPACHO,
                'TECNICO': p.aaData[k].TECNICO,
                'OBSERVACION_DESPACHO': p.aaData[k].OBSERVACION_DESPACHO,
                'REFERENCIA': p.aaData[k].REFERENCIA,
                'REFERENCIA2': p.aaData[k].REFERENCIA2,
                'OBSERVACION_DESPACHO2': p.aaData[k].OBSERVACION_DESPACHO2,
                'HORA_AGENDA': p.aaData[k].HORA_AGENDA,
                'CALLE': p.aaData[k].CALLE,
                'NUMERO': p.aaData[k].NUMERO,
                'SEMAFORO': p.aaData[k].SEMAFORO
              }]).draw();
            }
          }

          await ingresaModificacionesOTDth(p);

          var filtro = '';

          table.columns('13').search(filtro).draw();
          table.columns('14').search(filtro).draw();
          table.columns('2').search(filtro).draw();
          table.columns('3').search(filtro).draw();
          table.columns('7').search(filtro).draw();
          table.columns('9').search(filtro).draw();
          table.columns('20').search(filtro).draw();
          table.columns('5').search(filtro).draw();
          table.columns('29').search(filtro).draw();


           var cuerpoRegionDTH = '';
           cuerpoRegionDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(13).data().unique().length; i++){
             if(table.column(13).data().unique()[i] !== null){
               cuerpoRegionDTH += '<option value="' + table.column(13).data().unique()[i] + '">' + table.column(13).data().unique()[i] + '</option>';
             }
           }
           $("#regionOtDTH").html(cuerpoRegionDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#regionOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoComunaDTH = '';
           cuerpoComunaDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(14).data().unique().length; i++){
             if(table.column(14).data().unique()[i] !== null){
               cuerpoComunaDTH += '<option value="' + table.column(14).data().unique()[i] + '">' + table.column(14).data().unique()[i] + '</option>';
             }
           }
           $("#comunaOtDTH").html(cuerpoComunaDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#comunaOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoOrigenDTH = '';
           cuerpoOrigenDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(2).data().unique().length; i++){
             if(table.column(2).data().unique()[i] !== null){
               cuerpoOrigenDTH += '<option value="' + table.column(2).data().unique()[i] + '">' + table.column(2).data().unique()[i] + '</option>';
             }
           }
           $("#origenOtDTH").html(cuerpoOrigenDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#origenOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoTTicketDTH = '';
           cuerpoTTicketDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(3).data().unique().length; i++){
             if(table.column(3).data().unique()[i] !== null){
               cuerpoTTicketDTH += '<option value="' + table.column(3).data().unique()[i] + '">' + table.column(3).data().unique()[i] + '</option>';
             }
           }
           $("#tticketOtDTH").html(cuerpoTTicketDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#tticketOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpofechaCompDTH = '';
           cuerpofechaCompDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(7).data().unique().length; i++){
             if(table.column(7).data().unique()[i] !== null){
               cuerpofechaCompDTH += '<option value="' + table.column(7).data().unique()[i] + '">' + table.column(7).data().unique()[i] + '</option>';
             }
           }
           $("#fechaCompOtDTH").html(cuerpofechaCompDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#fechaCompOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoRangoDTH = '';
           cuerpoRangoDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(9).data().unique().length; i++){
             if(table.column(9).data().unique()[i] !== null){
               cuerpoRangoDTH += '<option value="' + table.column(9).data().unique()[i] + '">' + table.column(9).data().unique()[i] + '</option>';
             }
           }
           $("#rangoOtDTH").html(cuerpoRangoDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#rangoOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoOtDTH = '';
           cuerpoOtDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(20).data().unique().length; i++){
             if(table.column(20).data().unique()[i] !== null){
               cuerpoOtDTH += '<option value="' + table.column(20).data().unique()[i] + '">' + table.column(20).data().unique()[i] + '</option>';
             }
           }
           $("#despachoOtDTH").html(cuerpoOtDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#despachoOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoEstadoDTH = '';
           cuerpoEstadoDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(5).data().unique().length; i++){
             if(table.column(5).data().unique()[i] !== null){
               cuerpoEstadoDTH += '<option value="' + table.column(5).data().unique()[i] + '">' + table.column(5).data().unique()[i] + '</option>';
             }
           }
           $("#estadoOtDTH").html(cuerpoEstadoDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#estadoOtDTH").select2('destroy').select2({
                 theme: "bootstrap"
             });
           }

           var cuerpoObsDTH = '';
           cuerpoObsDTH += '<option selected value="Todos">Todos</option>';
           for(var i = 0; i < table.column(29).data().unique().length; i++){
             if(table.column(29).data().unique()[i] !== null){
               cuerpoObsDTH += '<option value="' + table.column(29).data().unique()[i] + '">' + table.column(29).data().unique()[i] + '</option>';
             }
           }
           $("#semafotoOtDTH").html(cuerpoObsDTH);
           if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
             $("#semafotoOtDTH").select2({
                 theme: "bootstrap"
             });
           }
        }
      }
    });
});

$("#agregarOTDTH").unbind("click").click(async function(){
  var min = moment().format('YYYY/MM/DD');
  $.datetimepicker.setLocale('es');
  $('#fechaHoraIngOTDTH').datetimepicker({
    format: 'd-m-Y H:i',
    minDate: min.toString(),
    step: 01
  });
  await $.ajax({
    url:   'controller/datosDespachos.php',
    type:  'post',
    success: function (response2) {
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        var cuerpoDespachosDTH = '';
        for(var i = 0; i < p.aaData.length; i++){
          if(p.aaData[i].COMUNA === null){
            cuerpoDespachosDTH += '<option value="' + p.aaData[i].IDPERSONAL + '">' + p.aaData[i].DNI + ' - ' + p.aaData[i].NOMBRES + '  ' + p.aaData[i].APELLIDOS + ' (Comuna: )' + '</option>';
          }
          else{
            cuerpoDespachosDTH += '<option value="' + p.aaData[i].IDPERSONAL + '">' + p.aaData[i].DNI + ' - ' + p.aaData[i].NOMBRES + '  ' + p.aaData[i].APELLIDOS + ' (Comuna: ' + p.aaData[i].COMUNA +')' + '</option>';
          }
        }
        $("#despachoIngOTDTH").html(cuerpoDespachosDTH);
      }
    }
  });
  await $.ajax({
    url:   'controller/datosAreaFuncional.php',
    type:  'post',
    success: function (response2) {
      var cuerpoAF = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoAF += "<option value=" + p.aaData[i].IDAREAFUNCIONAL + ">&nbsp;&nbsp;" + p.aaData[i].COMUNA + ', Región: ' + p.aaData[i].CODIGOREGION + "</option>";
        }
        $("#comunaRegionIngOTDTH").html(cuerpoAF);
      }
    }
  });
  await $.ajax({
    url:   'controller/datosTipoTkOTDTH.php',
    type:  'post',
    success: function (response2) {
      var cuerpoTT = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoTT += "<option value=" + p.aaData[i].TIPO_TICKET + ">&nbsp;&nbsp;" + p.aaData[i].TIPO_TICKET + "</option>";
        }
        $("#tipoTkIngOTDTH").html(cuerpoTT);
      }
    }
  });
  var parametros = {
    'tipoTk': $("#tipoTkIngOTDTH").val()
  }
  await $.ajax({
    url:   'controller/datosTipoOTDTH.php',
    type:  'post',
    data:  parametros,
    success: function (response2) {
      var cuerpoTT = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoTT += "<option value=" + p.aaData[i].TIPO + ">&nbsp;&nbsp;" + p.aaData[i].TIPO + "</option>";
        }
        $("#tipoIngOTDTH").html(cuerpoTT);
      }
    }
  });
  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#despachoIngOTDTH").select2({
        theme: "bootstrap"
    });
    $("#comunaRegionIngOTDTH").select2({
        theme: "bootstrap"
    });
    $("#tipoTkIngOTDTH").select2({
        theme: "bootstrap"
    });
    $("#tipoIngOTDTH").select2({
        theme: "bootstrap"
    });
  }
  var h = $(window).height() - 200;
  $("#bodyIngOTDTH").css("height",h);
  $("#modalIngOTDTH").modal("show");
});

$("#tipoTkIngOTDTH").unbind("click").change(async function(){
  var parametros = {
    'tipoTk': $("#tipoTkIngOTDTH").val()
  }
  await $.ajax({
    url:   'controller/datosTipoOTDTH.php',
    type:  'post',
    data:  parametros,
    success: function (response2) {
      var cuerpoTT = "";
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        for(var i = 0; i < p.aaData.length; i++){
          cuerpoTT += "<option value=" + p.aaData[i].TIPO + ">&nbsp;&nbsp;" + p.aaData[i].TIPO + "</option>";
        }
        $("#tipoIngOTDTH").html(cuerpoTT);
      }
    }
  });
  if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#tipoIngOTDTH").select2('destroy').select2({
        theme: "bootstrap"
    });
  }
});
